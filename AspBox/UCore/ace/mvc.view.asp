<%
'######################################################################
'## mvc.view.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc View Block
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/03/21 3:05
'## Description :   AspBox Mvc View Block(MVC View模块)
'######################################################################

Class Cls_Mvc_View

	Public UI
	Private s_libdir, s_cores, s_nocores, s_ex
	Private o_lib
	Private b_core

	Private Sub Class_Initialize()
		s_ex 		= "view"
		b_core 		= False '是否预加载核心
		s_cores 	= "UI" '预定义核心
		s_nocores	= "" '指定不加载的预定义核心
		s_libdir	= AB.Mvc.BasePath & "view/"
		Init()
	End Sub

	Public Sub Init()
		On Error Resume Next
		Set o_lib = Server.CreateObject(AB.dictName)
		Core_Do "on", s_cores '加载预定义核心
		[Load] "" '载入指定核心
		On Error Goto 0
	End Sub

	Private Sub Class_Terminate()
		Core_Do "off", s_cores
		[Drop] ""
		ClearLib()
		Set o_lib = Nothing
	End Sub

	'-控制引擎总线
	Public Function [Ace]()
		Dim o : Set o = New Cls_Mvc_View
		Set [Ace] = o
		Set o = Nothing
	End Function

	Public Property Let ifCore(ByVal s)
		b_core = s
	End Property

	Public Property Get ifCore()
		ifCore = b_core
	End Property

	Public Property Let baseCore(ByVal p)
		s_cores = p
	End Property
	Public Property Get baseCore()
		baseCore = s_cores
	End Property

	Public Property Let noCore(ByVal p)
		s_nocores = p
	End Property
	Public Property Get noCore()
		noCore = s_nocores
	End Property

	Public Function HasCore(ByVal s)
		On Error Resume Next
		Dim b : b = False
		Dim f : f = s
		f = Lcase(AB.C.RegReplace(f,"^\[(.*)\]$","$1"))
		If LCase(TypeName(Eval("["&f&"]"))) = "cls_view_" & f Then
			b = True
		End IF
		HasCore = b
		On Error Goto 0
	End Function

	Private Sub Core_Do(ByVal t, ByVal s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim p, f, g, a
		Select Case t
			Case "on"
				IF b_core Then
					For i = 0 To Ubound(a_core)
						g = AB.C.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
						f = Trim(Lcase(g))
						a = AB.Pub.CoresArray(s_nocores)
						If Not AB.Pub.InArray(f, a) Then
							p = s_libdir & "view." & f & ".asp"
							'If Not AB.Pub.IsLoaded_Core(s_ex, f, o_lib) Then:Execute "Set [" & g & "] = New Cls_View_Obj":End IF
							If Not AB.Pub.IsLoaded_Core(s_ex, f, o_lib) Then
								If AB.C.isFile(p) Then
									AB.Pub.CoreInclude s_ex, f, o_lib, p
									Execute("Set o_lib(""" & f & """) = New Cls_View_" & g)
									If Lcase(TypeName(o_lib(""& f))) = "cls_view_"& f Then Execute "Set [" & g & "] = o_lib(""" & f & """)"
								End If
							End If
						End If
					Next
				End IF
			Case "off"
				For i = Ubound(a_core) To 0 Step -1
					a_core(i) = AB.C.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
					Execute "Set [" & a_core(i) & "] = Nothing"
					If o_lib.Exists(Lcase(a_core(i))) Then Execute "Set [" & o_lib(Lcase(a_core(i))) & "] = Nothing"
					o_lib.Remove(Lcase(a_core(i)))
				Next
		End Select
		On Error Goto 0
	End Sub

	Public Sub [Load](Byval s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim p, f, g
		For i = 0 To Ubound(a_core)
			If trim(a_core(i))<>"" Then
				g = AB.C.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
				f = Lcase(g)
				p = s_libdir & "view." & f & ".asp"
				If Not AB.Pub.IsLoaded_Core(s_ex, f, o_lib) Then
					If AB.C.isFile(p) Then
						AB.Pub.CoreInclude s_ex, f, o_lib, p
						Execute("Set o_lib(""" & f & """) = New Cls_View_" & g)
						If Lcase(TypeName(o_lib(""& f))) = "cls_view_"& f Then Execute "Set [" & g & "] = o_lib(""" & f & """)"
					End If
				End If
			End If
		Next
		On Error Goto 0
	End Sub

	Public Default Function [Lib](ByVal o)
		On Error Resume Next
		If isnull(o) or trim(o)="" Then : [Lib] = Empty : Exit Function : End If
		Dim loaded : loaded = False
		Dim p, f, g
		g = AB.C.RegReplace(Trim(o),"^\[(.*)\]$","$1")
		f = Lcase(g)
		p = s_libdir & "view." & f & ".asp"
		If o_lib.Exists(""& f) and Lcase(TypeName(o_lib(""& f))) = "cls_view_"& f Then
			loaded = True
			Set [Lib] = o_lib(""& f)
			Exit Function
		ElseIf Lcase(TypeName("View.[" & g & "]")) = "cls_view_"& f Then
			loaded = True
			'Execute("Set o_lib(""" & f & """) = New Cls_View_" & f)
			Execute("Set o_lib(""" & f & """) = View.[" & g & "]")
			Set [Lib] = o_lib(f)
			Exit Function
		Else
			loaded = False
			If AB.C.isFile(p) Then
				AB.Pub.CoreInclude s_ex, f, o_lib, p
				Execute("Set o_lib(""" & f & """) = New Cls_View_" & f)
			Else
				AB.setError "(File Not Found: """ & p & """)", 4
			End If
		End If
		IF IsObject(o_lib(f)) Then Set [Lib] = o_lib(f)
		IF Err.Number<>0 Or Not IsObject(o_lib(f)) Then : [Lib] = Empty : Err.Clear : End IF
		On Error Goto 0
	End Function

	Public Sub Use(ByVal s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim p, f, g, t
		For i = 0 To Ubound(a_core)
			If trim(a_core(i))<>"" Then
				g = AB.C.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
				f = Lcase(g)
				p = s_libdir & "view." & f & ".asp"
				IF Not AB.Pub.IsLoaded_Core(s_ex, f, o_lib) Then
					t = Eval("LCase(TypeName([" & g & "]))")
					If t = "cls_view_obj" Or t = "nothing" Or t = "empty" Or t = "null" Then
						If AB.C.isFile(p) Then
							AB.Pub.CoreInclude s_ex, f, o_lib, p
							Execute("Set o_lib(""" & f & """) = New Cls_View_" & g)
							If Lcase(TypeName(o_lib(""& f))) = "cls_view_"& f Then Execute "Set [" & g & "] = o_lib(""" & f & """)"
						Else
							AB.setError "(File Not Found: """ & p & """)", 4
						End If
					End If
				End IF
			End If
		Next
		On Error Goto 0
	End Sub

	Private Sub ClearLib()
		Dim i
		If AB.C.Has(o_lib) Then
			For Each i In o_lib
				Set o_lib(i) = Nothing
			Next
			o_lib.RemoveAll
		End If
	End Sub

	Public Sub [Drop](Byval s)
		On Error Resume Next
		If isnull(s) or trim(s)="" Then Exit Sub
		Dim a_core, i : a_core = Split(s,",")
		Dim f, g
		For i = 0 To Ubound(a_core)
			If trim(a_core(i))<>"" Then
				g = AB.C.RegReplace(Trim(a_core(i)),"^\[(.*)\]$","$1")
				f = Lcase(g)
				If IsObject(o_lib) and Lcase(TypeName(o_lib))="dictionary" Then
					If o_lib.Exists(""&f) Then
						If IsObject(o_lib(""&f)) Then Execute("Set o_lib(""" & f & """) = Nothing")
						o_lib.Remove(""&f)
					End If
				End If
				Execute "Set [" & g & "] = Nothing"
			End If
		Next
		On Error Goto 0
	End Sub

End Class

Class Cls_View_Obj : End Class
%>