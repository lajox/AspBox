<%
'######################################################################
'## service.check.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Mvc Service-Check Block
'## Version     :   v1.0
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/07/21 1:15
'## Description :   AspBox Mvc Service-Check Block(MVC服务器检测模块)
'######################################################################

Class Cls_Service_Check
	Private i

	Private Sub Class_Initialize()

	End Sub

	Private Sub Class_Terminate()

	End Sub

	'*************************************************
	'函数名：Service.Check.IsInstall
	'作  用：检查组件是否已经安装
	'参  数：obj ----组件名
	'*************************************************
	Public Function IsInstall(Byval s)
		IsInstall = AB.C.IsInstall(s)
	End Function

	'*************************************************
	' 函数名: Service.Check.RefererURL
	' 作  用: 返回来源地址
	'*************************************************
	Public Function RefererURL()
		RefererURL = Request.ServerVariables("HTTP_REFERER")
	End Function

	'*************************************************
	' 函数名: Service.Check.CheckPostSource
	' 作  用: 检验来源地址
	'*************************************************
	Public Function CheckPostSource()
		Dim server_v1,server_v2
		server_v1=Cstr(Request.ServerVariables("HTTP_REFERER"))
		server_v2=Cstr(Request.ServerVariables("SERVER_NAME"))
		If Mid(server_v1,8,Len(server_v2))=server_v2 Then
			CheckPostSource=True
		Else
			CheckPostSource=False
		End If
	End Function

	'*************************************************
	' 函数名: Service.Check.CurrentURL
	' 作  用: 返回当前地址
	'@ ab.use "mvc" : ab.c.print Mvc.Service.Lib("Check").CurrentURL '输出值如 http://localhost/test/index.asp
	'*************************************************
	Public Function CurrentURL()
		Dim port : port = LCase(Request.ServerVariables("Server_Port"))
		Dim page : page = LCase(Request.ServerVariables("Script_Name"))
		Dim query : query = LCase(Request.QueryString())
		Dim url
		If CStr(port) = "80" Then
			url = page
		Else
			url = ":" & port & page
		End If
		If query <> "" Then
			CurrentURL = "http://" & Request.ServerVariables("server_name") & url & "?" & query
		Else
			CurrentURL = "http://" & Request.ServerVariables("server_name") & url
		End If
	End Function

	'*************************************************
	' 函数名: Service.Check.GetIP
	' 作  用: 获取客户端IP
	'@ ab.use "mvc"
	'@ Service.use "Check"
	'@ ab.c.print Service.Check.GetIP '输出值如 127.0.0.1
	'*************************************************
	Public Function GetIP()
		Dim ip
		If RqSv("HTTP_X_FORWARDED_FOR")="" OR InStr(RqSv("HTTP_X_FORWARDED_FOR"),"unknown") > 0 Then
			ip = RqSv("REMOTE_ADDR")
		ElseIf InStr(RqSv("HTTP_X_FORWARDED_FOR"),",")> 0 Then
			ip = Mid(RqSv("HTTP_X_FORWARDED_FOR"),1,InStr(RqSv("HTTP_X_FORWARDED_FOR"),",")-1)
		ElseIf InStr(RqSv("HTTP_X_FORWARDED_FOR"),";")>0 Then
			ip = Mid(RqSv("HTTP_X_FORWARDED_FOR"),1,InStr(RqSv("HTTP_X_FORWARDED_FOR"),";")-1)
		Else
			ip = RqSv("HTTP_X_FORWARDED_FOR")
		End If
		ip = Trim(Mid(ip,1,30))
		If InStr(ip,".")=0 Then ip = "0.0.0.0"
		'ip = AB.C.GetIP()
		GetIP = ip
	End Function

	'*************************************************
	' 函数名: Service.Check.GetSelfName
	' 作  用: 获取当前访问的文件名
	'@ ab.use "mvc"
	'@ Service.use "Check"
	'@ ab.c.print Service.Check.GetSelfName '输出值如 test.asp
	'*************************************************
	Public Function GetSelfName()
		GetSelfName = Request.ServerVariables("PATH_TRANSLATED")
		GetSelfName = LCase(Mid(GetSelfName, InstrRev(GetSelfName, "\") + 1, Len(GetSelfName)))
	End Function

	'*************************************************
	'函数名：Service.Check.GetPhysicalPath
	'作  用：获取网站的物理磁盘位置
	'返回值：字符串
	'@ ab.use "mvc"
	'@ Service.use "Check"
	'@ ab.c.print Service.Check.GetPhysicalPath '输出值如 E:\website\
	'*************************************************
    Public Function GetPhysicalPath()
        GetPhysicalPath = Request.ServerVariables("APPL_PHYSICAL_PATH")
    End Function

	'*************************************************
	'函数名：Service.Check.GetPathTranslated
	'作  用：获取脚本文件的物理磁盘位置
	'返回值：字符串
	'@ ab.use "mvc"
	'@ Service.use "Check"
	'@ ab.c.print Service.Check.GetPathTranslated '输出值如 E:\website\test01\test.asp
	'*************************************************
    Public Function GetPathTranslated()
        GetPathTranslated = Request.ServerVariables("PATH_TRANSLATED")
    End Function

	'*************************************************
	'函数名：Service.Check.GetFileAbsPath
	'作  用：获取脚本文件的绝对路径
	'返回值：字符串
    '@ e.g. test01/test.asp
	'*************************************************
    Public Function GetFileAbsPath()
        GetFileAbsPath = Request.ServerVariables("PATH_INFO")
    End Function

	'*************************************************
	'函数名：Service.Check.GetServerSoft
	'作  用：获取服务器软件信息
	'返回值：字符串
    '@ e.g. Microsoft-IIS/6.0
	'*************************************************
    Public Function GetServerSoft()
        GetServerSoft = Request.ServerVariables("SERVER_SOFTWARE")
    End Function

	'*************************************************
	'函数名：Service.Check.GetWebPort
	'作  用：获取WEB端口
	'返回值：字符串
	'*************************************************
    Public Function GetWebPort()
        GetWebPort = Request.ServerVariables("SERVER_PORT")
    End Function

	'*************************************************
	'函数名：Service.Check.GetWebPort
	'作  用：获取服务器名称
	'返回值：字符串
	'*************************************************
    Public Function GetServerName()
        GetServerName = Request.ServerVariables("SERVER_NAME")
    End Function

	'*************************************************
	'函数名：Service.Check.GetWebPort
	'作  用：获取服务器处理器数量
	'返回值：字符串
	'*************************************************
    Public Function GetCPUNumber()
        GetCPUNumber = Request.ServerVariables("NUMBER_OF_PROCESSORS")
    End Function

	'*************************************************
	'函数名：Service.Check.GetWebPort
	'作  用：获取脚本解译引擎信息
	'返回值：字符串
	'*************************************************
    Public Function GetScriptEngine()
        GetScriptEngine = ScriptEngine & "/"& ScriptEngineMajorVersion & "." & ScriptEngineMinorVersion & "." & ScriptEngineBuildVersion
    End Function

	'*************************************************
	'函数名：Service.Check.GetOS
	'作  用：获取客户端操作系统版本
	'返回值：字符串
	'*************************************************
	Public Function GetOS()
		On Error Resume Next
		Dim OS : OS = Request.ServerVariables("HTTP_USER_AGENT")
		If Instr(OS,"Windows NT 5.2") Then
			OS = "Windows 2003"
		ElseIf Instr(OS,"Windows NT 5.0") Then
			OS="Windows Server 2000"
		ElseIf Instr(OS,"Windows NT 5.1") Then
			OS = "Windows XP"
		ElseIf Instr(OS,"Windows NT 6.1") Then
			OS = "Windows 7"
		ElseIf Instr(OS,"Windows NT 6.2") Then
			OS = "Windows 8"
		ElseIf Instr(OS,"Windows NT 6.0") Then
			OS = "Windows Server 2008"
		ElseIf Instr(OS,"Windows NT 6") Then
			OS = "Windows Vista"
		ElseIf Instr(OS,"Windows NT") Then
			OS = "Windows NT"
		ElseIf Instr(OS,"Windows 9") Then
			OS = "Windows 9x"
		Elseif Instr(OS,"unix") Or InStr(OS,"linux") Or InStr(OS,"SunOS") Or InStr(OS,"BSD") Then
			OS = "类Unix"
		ElseIf Instr(OS,"Mac") Then
			OS = "Mac"
		Else
			OS = "Other"
		End If
		GetOS = OS
		On Error Goto 0
	End Function

	'*************************************************
	'函数名：Service.Check.GetBS(p)
	'作  用：获取服务端浏览器信息
	'返回值：字符串
	'参  数: p=1返回浏览器名称; p=2返回浏览器版本; p=0返回浏览器名称及版本;
	'@ e.g.
	'@ ab.use "mvc"
	'@ Service.use "Check"
	'@ ab.c.print Service.Check.GetBS(2)
	'*************************************************
	Function GetBS(Byval p)
		Dim Agent,Browser,Version,tmpstr
		Agent=Request.ServerVariables("HTTP_USER_AGENT")
		Agent=Split(Agent,";")
		If InStr(Agent(1),"MSIE")>0 Then
			Browser="MS Internet Explorer"
			Version=Trim(Left(Replace(Agent(1),"MSIE",""),6))
		ElseIf InStr(Agent(4),"Netscape")>0 Then 
			Browser="Netscape"
			tmpstr=Split(Agent(4),"/")
			Version=tmpstr(UBound(tmpstr))
		ElseIf InStr(Agent(4),"rv:")>0 Then
			Browser="Mozilla"
			tmpstr=Split(Agent(4),":")
			Version=tmpstr(UBound(tmpstr))
			If InStr(Version,")") > 0 Then 
				tmpstr=Split(Version,")")
				Version=tmpstr(0)
			End If
		End If
		Select Case Cstr(p)
			Case 0 : GetBS = ""& Browser &" "& Version &""
			Case 1 : GetBS = Browser
			Case 2 : GetBS = Version
		End Select
	End Function

	Function GetBrowser
		On Error Resume Next
		Dim agent,vBS
		agent=Request.ServerVariables("HTTP_USER_AGENT")
		if Instr(agent,"NetCaptor 6.5.0")>0 then
			vBS="NetCaptor 6.5.0"
		elseif Instr(agent,"NetCaptor 6.5.0RC1")>0 then
			vBS="NetCaptor 6.5.0RC1"
		elseif Instr(agent,"NetCaptor 6.5.PB1")>0 then
			vBS="NetCaptor 6.5.PB1"
		elseif Instr(agent,"MSIE 6.0")>0 then
			vBS="Internet Explorer 6.0"
		elseif Instr(agent,"MSIE 6.0b")>0 then
			vBS="Internet Explorer 6.0b"
		elseif Instr(agent,"MSIE 5.5")>0 then
			vBS="Internet Explorer 5.5"
		elseif Instr(agent,"MSIE 5.01")>0 then
			vBS="Internet Explorer 5.01"
		elseif Instr(agent,"MSIE 5.0")>0 then
			vBS="Internet Explorer 5.00"
		elseif Instr(agent,"MSIE 4.0")>0 then
			vBS="Internet Explorer 4.01"
		elseif Instr(agent,"Netscape")>0 then
			vBS="Netscape"
		elseif Instr(agent,"Opera")>0 then
			vBS="Opera"
		elseif Instr(agent,"MyIe 3.1")>0 then
			vBS="MyIe 3.1"
		else
			vBS="Other"
		end if
		GetBrowser = vBS
		On Error Goto 0
	End Function

	Function BsDict()
		On Error Resume Next
		Dim dict : Set dict = Server.CreateObject(AB.DictName)
		Dim bc : Set bc = Server.CreateObject("MSWC.BrowserType")
		dict.add "Browser", bc.Browser
		dict.add "Platform", bc.Platform
		dict.add "Version", bc.Version 'XP以前的版本显示有误
		dict.add "Beta", bc.Beta
		dict.add "Frames", bc.Frames
		dict.add "Tables", bc.Tables
		dict.add "Backgroundsounds", bc.Backgroundsounds
		dict.add "Cookies", bc.Cookies
		dict.add "VBScript", bc.VBScript
		dict.add "JavaScript", bc.JavaScript
		dict.add "ActiveXControls", bc.ActiveXControls
		dict.add "Cdf", bc.Cdf
		dict.add "Javaapplets", bc.Javaapplets
		Set BsDict = dict
		On Error Goto 0
	End Function
End Class
%>