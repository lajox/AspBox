<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include virtual="/Inc/AspBox/Cls_AB.asp" -->
<%
'定义标题
Dim WebTitle, WebKeywords, WebDesription, WebMessage
WebTitle       = "AB.Tpl Demo"
WebKeywords    = "AB.Tpl(ASP模板引擎)"
WebDesription  = "AB.Tpl(ASP模板引擎)"

'数据库连接
Dim conn,rs,sql
Set conn = Server.createobject("ADODB.Connection")
Set rs = server.createobject("ADODB.RecordSet")
conn.Open "Provider=Microsoft.jet.OLEDB.4.0;Data Source=" & Server.MapPath("data.mdb")
sql="select * from log where id>0 order by id desc"
If Err.Number=0 Then
  rs.Open sql,conn,1,1 
  rs.PageSize = 1        '分页行数
  If Not rs.eof Then rs.AbsolutePage = 1
End If
Err.Clear

'--------------------------------------------------

AB.Debug = True
Dim txt,i,j,html
txt = "这篇文档是AB的模板类的测试文档和示例文件"

'加载tpl核心
Dim tpl
Set tpl = AB.Lib("tpl")

'允许在模板文件中使用ASP代码
'tpl.AspEnable = True

'模板文件所在文件夹，支持绝对路径和相对路径
tpl.FilePath = "tpl/"
'模板文件中可以用{#include}标签包含无限层次的子模板，也都支持相对路径和绝对路径，请参考html文件夹内的模板文件

'如何处理未替换的标签,"keep"-保留，"remove"-移除，"comment"-转成注释
'tpl.TagUnknown = "comment"

'模板标签的样式，默认为"{*}"，*号为标签名
'tpl.TagMask = "{$*$}"

'加载模板
tpl.Load "tpl.html"

'也可以用下面这种方式加载模板
'tpl.File = "tpl.html"

'开始解析标签，MakeTag可以快速生成html标签
tpl "author", "Lajox, Nasrick"
tpl "keywords", "AspBox, AB"
tpl "description", "This is a AspBox TPL Sample."

'将标签替换为副模板
tpl.TagFile "style", "style.html"

tpl "jsfile", tpl.MakeTag("js","tpl/inc/inc.js")
tpl "cssfile", tpl.MakeTag("css","tpl/inc/css/main.css|tpl/inc/css/page.css")
tpl "css", tpl.MakeTag("css","tpl/inc/css.css")

tpl "title", "AspBox 模板类测试页"
tpl "subtitle", txt
tpl "color", "#F60"

If Hour(Now)>=6 Then
'追加标签内容：
tpl.Append "subtitle", " <small>[<font color=red>6</font>点之后显示]</small>"
End If

'追加标签内容：
tpl.TagStr "iftag", "{#if @count!=''}共有 <strong>{count}</strong> 条数据，{/#if}以下是其中的{#if @top>0}前 <strong>{top}</strong> 条{#else}数据{/#if}"
tpl "top", 0
tpl "count", 12
'tpl "top", 5

'开始循环
For i = 1 to 3
AB.Tpl "A.title", "A标题" & i
AB.Tpl "A.addtime", AB.C.DateTime(Now(),"y/mm/dd")
'更新本次循环数据，每次循环后必须调用此方法
AB.Tpl.Update "A"
Next

'嵌套循环演示，嵌套可以无限层的，这是父循环
For i = 1 to 4
AB.Tpl "B.title", "B标题" & i & " | "
'Demo中的 B. 这个前缀不是必须的，只是为了代码方便阅读
AB.Tpl "id", i+10
AB.Tpl "addtime", AB.C.DateTime(Now()-5,"y/m/d/ h:i:s")
'这是子循环
For j = 20 to 23
'替换标签
AB.Tpl "page.list", " "&i&">"&j
'更新本次循环数据
AB.Tpl.Update "page"
Next
'更新本次循环数据
AB.Tpl.Update "B"
Next

'将替换完毕的html输出至浏览器
tpl.Show

'或者，也可以生成静态页
'得到替换完毕的html代码
html = tpl.GetHtml
'生成静态页
'AB.Use "Fso"
'Call AB.Fso.CreateFile("demo.tpl.html",html)

'释放Tpl对象
Set Tpl = Nothing

'--------------------------------------------------

rs.close   : Set rs = Nothing   
conn.close : Set conn = Nothing
%>