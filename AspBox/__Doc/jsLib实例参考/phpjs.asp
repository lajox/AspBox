<!--#include file="inc/AspBox/Cls_AB.asp" -->
<%
AB.Use "jsLib"
Dim jsLib : Set jsLib = AB.jsLib.New
jsLib.Inc("phpjs.js")
Dim jso : Set jso = jsLib.Object
Dim str : str = ""

ab.c.printcn jso.sha1("123456") '@return: 7c4a8d09ca3762af61e59520943dc26494f8941b
ab.c.printcn jso.md5("123456") '@return: e10adc3949ba59abbe56e057f20f883e
ab.c.printcn jso.abs(-1.54) '@return: 1.54
ab.c.printcn jso.max(1, 3, 5, 6, 7) '@return: 7
ab.c.printcn jso.ucwords("hello kitty") '@return: Hello Kitty
ab.c.printcn jso.ucfirst("hello kitty") '@return: Hello kitty
ab.c.printcn jso.strtolower("aBcdEFG123") '@return: abcdefg123
ab.c.printcn jso.trim(" ab cd efg ") '@return: ab cd efg
ab.c.printcn jso.time() '@return current UNIX timestamp like 1356004269
ab.c.printcn jso.strtotime("+1 day", jso.time()) '@return current UNIX timestamp like 1356090669
ab.c.printcn jso.date("Y-m-d H:i:s", jso.time()) '@return time format like 2012-12-20 19:55:11
ab.c.printcn jso.strstr("name@example.com", "@") '@return: @example.com
ab.c.printcn jso.strrev("abcde1234") '@return: 4321edcba
ab.c.printcn jso.strlen("abcdefg") '@return: 7
%>