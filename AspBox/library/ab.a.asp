<%
'######################################################################
'## ab.a.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Array Extensive Class
'## Version     :   v1.0.2
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2015/12/05 09:48
'## Description :   AspBox数组(Array)操作模块
'######################################################################

Class Cls_AB_A

	Private Sub Class_Initialize()
		'AB.Use "H"
	End Sub

	Private Sub Class_Terminate()

	End Sub

	'------------------------------------------------------------------------------------------
	'# AB.A.Max(arr)
	'# @return: Integer / Double
	'# @dowhat: 取出数组元素最大值(所有数组元素均须为数字类型)
	'--DESC------------------------------------------------------------------------------------
	'# @param arr: [array] (数组)
	'--DEMO------------------------------------------------------------------------------------
	'# ab.c.print AB.A.Max(array(1, 5.5, 3.2)) '=> 5.5
	'------------------------------------------------------------------------------------------

	Public Function Max(ByVal arr)
		Dim tmp
		tmp = Me.rSort(arr)
		Max = tmp(0)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.A.Min(arr)
	'# @return: Integer / Double
	'# @dowhat: 取出数组元素最小值(所有数组元素均须为数字类型)
	'--DESC------------------------------------------------------------------------------------
	'# @param arr: [array] (数组)
	'--DEMO------------------------------------------------------------------------------------
	'# ab.c.print AB.A.Min(array(1, 5.5, 3.2)) '=> 1
	'------------------------------------------------------------------------------------------

	Public Function Min(ByVal arr)
		Dim tmp
		tmp = Me.Sort(arr)
		Min = tmp(0)
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.A.Sum(arr)
	'# @return: Integer / Double
	'# @dowhat: 计算所有数组元素相加总和
	'--DESC------------------------------------------------------------------------------------
	'# @param arr: [array] (数组) 若参数为空数组或非数组则函数值返回空值
	'--DEMO------------------------------------------------------------------------------------
	'# ab.trace AB.A.Sum(array(1, 2.5, 1.6)) '=> 5.1
	'------------------------------------------------------------------------------------------

	Public Function Sum(ByVal arr)
		Dim tmp, item
		If IsArray(arr) and Me.Len(arr) > 0 Then
			tmp = 0
			For Each item in arr
				tmp = tmp + item
			Next
		End If
		Sum = tmp
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.A.Avg(arr)
	'# @return: Integer / Double
	'# @dowhat: 计算所有数组元素的平均数
	'--DESC------------------------------------------------------------------------------------
	'# @param arr: [array] (数组) 若参数为空数组或非数组则函数值返回空值
	'--DEMO------------------------------------------------------------------------------------
	'# ab.trace AB.A.Avg(array(1, 2.5, 1.6)) '=> 1.7
	'------------------------------------------------------------------------------------------

	Public Function Avg(ByVal arr)
		Dim tmp, n, item
		If IsArray(arr) and Me.Len(arr) > 0 Then
			tmp = 0 : n = 0
			For Each item in arr
				tmp = tmp + item
				n = n + 1
			Next
			If n > 0 Then tmp = tmp / n Else tmp = ""
		End If
		Avg = tmp
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.A.First(arr)
	'# @return: Anything (任意值)
	'# @dowhat: 取得第一个元素值
	'--DESC------------------------------------------------------------------------------------
	'# @param arr: [array] (数组) 若空数组或非数组则函数值返回 空值
	'--DEMO------------------------------------------------------------------------------------
	'# Dim o : Set o = AB.Dict '字典对象
	'# ab.trace AB.A.First(array("abcd", o, 5)) '=> "abcd"
	'# Dim x : Set x = AB.A.First(array(o, "abcd", 5)) '=> 对象 o
	'# ab.trace x
	'------------------------------------------------------------------------------------------

	Public Function First(ByVal arr)
		First = ""
		If IsArray(arr) and Me.Len(arr) > 0 Then
			If IsObject(arr(0)) Then : Set First = arr(0) : Else : First = arr(0) : End if
		End If
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.A.End(arr)
	'# @alias: AB.A.Last(arr)
	'# @return: Anything (任意值)
	'# @dowhat: 取得最后一个元素值
	'--DESC------------------------------------------------------------------------------------
	'# @param arr: [array] (数组) 若空数组或非数组则函数值返回 空值
	'--DEMO------------------------------------------------------------------------------------
	'# Dim o : Set o = AB.Dict '字典对象
	'# ab.trace AB.A.End(array("abcd", o, 5)) '=> 5
	'# Dim x : Set x = AB.A.End(array("abcd", 5, o)) '=> 对象 o
	'# ab.trace x
	'------------------------------------------------------------------------------------------

	Public Function [End](ByVal arr)
		[End] = ""
		If IsArray(arr) and Me.Len(arr) > 0 Then
			Dim n : n = UBound(arr)
			If IsObject(arr(n)) Then : Set [End] = arr(n) : Else : [End] = arr(n) : End if
		End If
	End Function
	Public Function Last(ByVal arr)
		If IsObject(Me.End(arr)) Then : Set Last = Me.End(arr) : Else : Last = Me.End(arr) : End if
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.A.Clone(arr)
	'# @return: array
	'# @dowhat: 实现数组的克隆(拷贝)
	'# 			对数组 arr 进行克隆，生成新数组(动态数组,可随时新增元素)
	'# 			语法： arrNew = AB.A.Clone(arr)
	'--DESC------------------------------------------------------------------------------------
	'# @param arr: [array] (源数组)
	'--DEMO------------------------------------------------------------------------------------
	'# dim a1, a2
	'# a1 = array("a","b","c")
	'# a2 = array("x","y",3)
	'# a2 = ab.a.clone(a1)
	'# ab.trace(a2) '=> Array("a","b","c")
	'------------------------------------------------------------------------------------------

	Public Function Clone(ByVal arr)
		Dim i, a()
		If Me.Len(arr)<=0 Then : Clone = arr: Exit Function: End If
		For i = LBound(arr) To UBound(arr)
			Redim Preserve a(i)
			If IsObject(arr(i)) Then Set a(i) = arr(i) Else a(i) = arr(i)
		Next
		Clone = a
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.UnShift(arr, s)
	'# @return: array
	'# @dowhat: 向数组中从前压入元素
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'# @param s   [string] : 字符串元素
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.UnShift(array("a", "b", "c"), "d") '=> Array("d", "a", "b", "c")
	'-------------------------------------------------------------------------------------------

	Public Function UnShift(ByVal arr, ByVal s)
		Dim i, a()
		If Me.Len(arr)<=0 Then
			Redim Preserve a(0)
			If IsObject(s) Then Set a(0) = s Else a(0) = s
			UnShift = a
			Exit Function
		End If
		Redim Preserve a(LBound(arr)) : If IsObject(s) Then Set a(LBound(arr)) = s Else a(LBound(arr)) = s
		For i = LBound(arr) To UBound(arr)
			Redim Preserve a(i+1)
			If IsObject(arr(i)) Then Set a(i+1) = arr(i) Else a(i+1) = arr(i)
		Next
		UnShift = a
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Shift(arr, s)
	'# @return: array
	'# @dowhat: 向数组中从前删除元素
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'# @param s   [string] : 字符串元素
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Shift(array("a", "b", "c", "d")) '=> Array("b", "c", "d")
	'-------------------------------------------------------------------------------------------

	Public Function Shift(ByVal arr)
		Dim i, a()
		If Me.Len(arr)<=0 Then : Shift = Array(): Exit Function : End If
		For i = LBound(arr)+1 To UBound(arr)
			Redim Preserve a(i-1)
			If IsObject(arr(i)) Then Set a(i-1) = arr(i) Else a(i-1) = arr(i)
		Next
		Shift = a
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Push(arr, s)
	'# @return: array
	'# @dowhat: 向数组中从后压入元素
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'# @param s   [string] : 字符串元素
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Push(array("a", "b", "c"), "d") '=> Array("a", "b", "c", "d")
	'-------------------------------------------------------------------------------------------

	Public Function Push(ByVal arr, ByVal s)
		Dim i, a : a = Me.Clone(arr)
		If Me.Len(arr)<=0 Then
			Redim Preserve a(0)
			If IsObject(s) Then Set a(0) = s Else a(0) = s
			Push = a
			Exit Function
		End If
		Redim Preserve a(UBound(arr)+1)
		If IsObject(s) Then Set a(UBound(arr)+1) = s Else a(UBound(arr)+1) = s
		Push = a
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Pop(arr)
	'# @return: array
	'# @dowhat: 向数组中从后删除元素
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Pop(array("a", "b", "c", "d")) '=> Array("a", "b", "c")
	'-------------------------------------------------------------------------------------------

	Public Function Pop(ByVal arr)
		Dim i, a()
		If Me.Len(arr)<=1 Then : Pop = Array(): Exit Function : End If
		For i = LBound(arr) To UBound(arr)-1
			Redim Preserve a(i)
			If IsObject(arr(i)) Then Set a(i) = arr(i) Else a(i) = arr(i)
		Next
		Pop = a
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Strip(str)
	'# @return: array
	'# @dowhat: 去除字符串最右边和最左边的(,)符号,并返回以(,)分割成的数组
	'# 			去除如(1,2,3,)或(,1,2,3)最右边和最左边的(,)符号,并返回以(,)分割成的数组
	'--DESC-------------------------------------------------------------------------------------
	'# @param str   [string] : 字符串 str as a string such as "1,2,3,"
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Strip("a,b,c,d,") '=> Array("a", "b", "c", "d")
	'-------------------------------------------------------------------------------------------

	Public Function Strip(ByVal Str)
		AB.Use "H" : If IsArray(Str) Then Str = Me.ToString(Str)
		If Left(Str, 1) = "," Then Str = Right(Str, AB.H.Len(Str) -1)
		If Right(Str, 1) = "," Then Str = Left(Str, AB.H.Len(Str) -1)
		Str = Me.ToArray(Str)
		Strip = Str
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Walk(arr, f)
	'# @return: array
	'# @dowhat: 对数组内元素执行函数后返回新数组
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr 	[array] : (数组)
	'# @param f 	[string | mix-array] : (字符串 或 混合型数组)
	'# 				参数f是字符串，为要执行的函数名
	'# 				参数f是数组，则第一个元素为要执行的函数名，第二个元素做为执行函数的第二个以上参数数组
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Walk(array("ab", "cde", "fghi"), "len") '=> Array(2, 3, 4)
	'# ab.trace ab.a.Walk(array("ab", "cde", "fghi"), array("len")) '=> Array(2, 3, 4)
	'# Function foo(a, b, c) : foo = a + b + c : End Function
	'# ab.trace ab.a.Walk(array(1, 2, 3), array("foo", array(2, 3)) ) '=> Array(6, 7, 8)
	'-------------------------------------------------------------------------------------------

	Public Function Walk(ByVal arr, ByVal f)
		On Error Resume Next
		Dim callback, e, tmp, i, j, estr, fstr, a() : e = "" : tmp = "" : fstr = "" : i = 0 : AB.Use "H"
		If IsArray(f) Then
			If UBound(f)>-1 Then If VarType(f(0)) = vbString Then callback = f(0)
			If UBound(f)>0 Then
				If Not IsArray(f(1)) Then fstr = ", f(1)"
				If IsArray(f(1)) Then
					For i = 0 To UBound(f(1))
						fstr = fstr & ", f(1)("& i &")"
					Next
				End If
			End If
		End If
		If VarType(f) = vbString Then callback = f
		If Lcase(Trim(callback))="len" Then callback = "AB.H.Len"
		If Lcase(Trim(callback))="cstr" Then callback = "AB.H.Cstr"
		If IsNull(callback) Or Trim(callback)="" Then : Walk = arr : Exit Function : End If
		If Me.Len(arr)=-1 Then '非数组
			estr = "tmp=" & callback & "(arr " & fstr & ")"
			Execute(estr)
			Walk = tmp
		ElseIf Me.Len(arr)=0 Then '动态数组(空元素)
			Walk = arr
		Else '数组(元素非空)
			i = LBound(arr)
			For Each e in arr
				estr = "tmp=" & callback & "(e " & fstr & ")"
				Execute(estr)
				Redim Preserve a(i)
				a(i) = tmp
				i = i + 1
			Next
			Walk = a
		End If
		On Error GoTo 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Sub(arr, start, over) 截取元素(按索引范围)
	'# @return: array
	'# @dowhat: (根据索引范围:从开始索引 start 到 结束索引 over)从一个数组中截取一部分元素(一个或多个)并返回新数组
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr 		[Array]   : array
	'# @param start 	[Integer] : start index
	'# @param over	 	[Integer] : over index
	'# 					当结束索引 over 值为-1时, 表示截取元素范围从开始索引 index 开始直到结束。
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Sub(array("a","b","c","d"), 0, 0) '=> Array("a")
	'# ab.trace ab.a.Sub(array("a","b","c","d"), 1, 2) '=> Array("b", "c")
	'# ab.trace ab.a.Sub(array("a","b","c","d"), -1, 2) '=> Array("a","b","c")
	'# ab.trace ab.a.Sub(array("a","b","c","d"), 0, -1) '=> Array("a", "b", "c", "d")
	'-------------------------------------------------------------------------------------------

	Public Function [Sub](ByVal arr, ByVal start, ByVal over)
		On Error Resume Next
		If Me.Len(arr)<=0 Then : [Sub] = Array(): Exit Function : End If
		Dim i, k, tmp, a()
		If AB.C.isInt(start) Then
			Dim n:n=CLng(start) : Dim z:z=CLng(over) : Dim m:m=UBound(arr) : Dim p:p=LBound(arr)
			If z>m Or z=-1 Then z=m
			If n<0 Then n=0
			For i=n To z
				Redim Preserve a(k)
				If i<=m Then
					If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
				End If
				k = k+1
			Next
			tmp = a
			If k=0 Then tmp = Array()
		Else
			tmp = Array()
		End If
		[Sub] = tmp
		On Error GoTo 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Slice(arr, start, length) 截取元素(从起始索引按长度截取)
	'# @return: array
	'# @dowhat: (根据指定的索引范围)从一个数组中截取一部分元素(一个或多个)并返回新数组
	'# 			start>=0时,为正常截取(从索引值为start处开始截取)返回数组
	'# 			strat<0时,从索引值[数组长度m-(-strat)处]开始截取返回数组
	'# 			length>0时,从索引值start处开始截取到start+length处截取返回数组
	'# 			length=0时,从索引值start处开始截取到结束截取返回数组
	'# 			length<0时,从索引值start处开始向左截取到start-(-length)结束截取返回数组
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr 		[Array]   : array
	'# @param start 	[Integer] : start index
	'# @param length 	[Integer] : length
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Slice(array("a","b","c","d"), 1, 2) '=> Array("b", "c")
	'# ab.trace ab.a.Slice(array("a","b","c","d"), 2, 5) '=> Array("c", "d")
	'# ab.trace ab.a.Slice(array("a","b","c","d"), 0, 2) '=> Array("a", "b")
	'# ab.trace ab.a.Slice(array("a","b","c","d"), 0, 0) '=> Array("a", "b", "c", "d")
	'# ab.trace ab.a.Slice(array("a","b","c","d","e"), 3, -2) '=> Array("d", "c", "b")
	'-------------------------------------------------------------------------------------------

	Public Function Slice(ByVal arr, ByVal start, ByVal length)
		On Error Resume Next
		If Me.Len(arr)<=0 Then : Slice = Array(): Exit Function : End If
		Dim i, k, tmp, a(), over
		If AB.C.isInt(start) Then
			Dim n:n=CLng(start) : Dim l:l=CLng(length) : Dim m:m=UBound(arr) : over = m : Dim p:p=LBound(arr)
			If n<0 Then n=m-Abs(n)
			If n<0 Then n=0
			If n>=0 Then
				If m<n Then: Slice = Array(): Exit Function : End If
				k=0 : over = m
				If l>=0 Then
					If l=0 Or n+l-1>=m Then
						over = m
					Else
						over = n+l-1
					End If
					For i=n To over
						Redim Preserve a(k)
						If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
						k = k+1
					Next
				Else
					over = n+l
					If over<p Then over=p
					For i=n To over Step -1
						Redim Preserve a(k)
						If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
						k = k+1
					Next
				End If
				tmp = a
				If k=0 Then tmp = Array()
			End If
		Else
			tmp = Array()
		End If
		Slice = tmp
		On Error GoTo 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Splice(arr, start, length) 移除元素
	'# @return: array
	'# @dowhat: (根据指定的索引范围)从一个数组中移除一部分元素(一个或多个)并返回新数组
	'# 			start>=0时,为正常(从索引值为start处开始)部分删除后返回的数组
	'# 			start<0时,从索引值[数组长度m-(-start)处]开始部分删除后返回的数组
	'# 			length>0时,从索引值start处开始到start+length处截取返回的数组
	'# 			length=0时,从索引值start处开始到结束部分删除后返回的数组
	'# 			length<0时,从索引值start处开始向左到start-(-length)结束部分删除后返回的数组
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr 		[Array]   : array
	'# @param start 	[Integer] : start index
	'# @param length 	[Integer] : length
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Splice(array("a","b","c","d"), 1, 2) '=> Array("a", "d")
	'# ab.trace ab.a.Splice(array("a","b","c","d"), 2, 5) '=> Array("a", "b")
	'# ab.trace ab.a.Splice(array("a","b","c","d"), 0, 2) '=> Array("c", "d")
	'# ab.trace ab.a.Splice(array("a","b","c","d"), 0, 0) '=> Array()
	'# ab.trace ab.a.Splice(array("a","b","c","d","e"), 3, -1) '=> Array("a", "b", "e")
	'-------------------------------------------------------------------------------------------

	Public Function Splice(ByVal arr, ByVal start, ByVal length)
		On Error Resume Next
		If Me.Len(arr)<=0 Then : Splice = Array(): Exit Function : End If
		Dim i, k, tmp, a(), over
		If AB.C.isInt(start) Then
			Dim n:n=CLng(start) : Dim l:l=CLng(length) : Dim m:m=UBound(arr) : over = m : Dim p:p=LBound(arr)
			If n<0 Then n=m-Abs(n)
			If n<0 Then n=0
			If n>=0 Then
				If m<n Then: Splice = arr: Exit Function : End If
				k=0 : over = m
				If l>=0 Then
					If l=0 Or n+l-1>=m Then
						over = m
					Else
						over = n+l-1
					End If
					For i=p To m
						If i<n Or i>over Then
							Redim Preserve a(k)
							If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
							k = k+1
						End If
					Next
				Else
					over = n+l
					If over<p Then over=p
					For i=p To m
						If (i<over) or (i>n) Then
							Redim Preserve a(k)
							If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
							k = k+1
						End If
					Next
				End If
				tmp = a
				If k=0 Then tmp = Array()
			End If
		Else
			tmp = arr
		End If
		Splice = tmp
		On Error GoTo 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.SpliceX(arr, start, length, elem) 替换元素
	'# @return: array
	'# @dowhat: (根据指定的索引范围)从一个数组中将一部分元素(一个或多个)用填充元素代替并返回新数组
	'# 			start>=0时,为正常(从索引值为start处开始)部分删除后返回的数组
	'# 			start<0时,从索引值[数组长度m-(-start)处]开始部分删除后返回的数组
	'# 			length>0时,从索引值start处开始到start+length处截取返回的数组
	'# 			length=0时,从索引值start处开始到结束部分删除后返回的数组
	'# 			length<0时,从索引值start处开始向左到start-(-length)结束部分删除后返回的数组
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr 		[Array]   : array
	'# @param start 	[Integer] : start index
	'# @param length 	[Integer] : length
	'# @param elem	 	[Array | Object|String|Other.. | Null] : 填充元素
	'# 					当 elem参数 为 Array 时, 表示填充批量元素组
	'# 					当 elem参数 为 Object|String|Other.. 时, 表示填充单个元素
	'# 					当 elem参数 为 null 时, 不填充元素, 等效于 AB.A.Splice(arr, start, length) 方法
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.SpliceX(array("a","b","c","d"), 1, 2, array("x","y")) '=> Array("a", "x", "y", "d")
	'# ab.trace ab.a.SpliceX(array("a","b","c","d"), 2, 5, array("x","y")) '=> Array("a", "b", "x", "y")
	'# ab.trace ab.a.SpliceX(array("a","b","c","d"), 0, 2, array("x","y")) '=> Array("x", "y", "c", "d")
	'# ab.trace ab.a.SpliceX(array("a","b","c","d"), 0, 0, array("x","y")) '=> Array("x", "y")
	'# ab.trace ab.a.SpliceX(array("a","b","c","d","e"), 3, -1, array("x","y")) '=> Array("a", "b", "x", "y", "e")
	'# ab.trace ab.a.SpliceX(array("a","b","c","d","e"), -1, 0, "x") '=> Array("a","b","c","d","x")
	'# ab.trace ab.a.Concat(array("a","b","c","d","e"), array("x","y")) '=> Array("a","b","c","d","e","x","y")
	'# ab.trace ab.a.Fill(array("a","b","c","d","e"), 1, "x") '=> Array("a","x","b","c","d","e")
	'# 'ab.trace ab.a.Fill(array("a","b","c","d","e"), 1, Array("x","y")) '=> Array("a","x","y","b","c","d","e")
	'-------------------------------------------------------------------------------------------

	Public Function SpliceX(ByVal arr, ByVal start, ByVal length, ByVal elem)
		On Error Resume Next
		If Me.Len(arr)<=0 Then : Splice = Array(): Exit Function : End If
		Dim i, k, tmp, a(), over, e
		If AB.C.isInt(start) Then
			Dim n:n=CLng(start) : Dim l:l=CLng(length) : Dim m:m=UBound(arr) : over = m : Dim p:p=LBound(arr)
			If n<0 Then n=m-Abs(n)
			If n<0 Then n=0
			If n>=0 Then
				If m<n Then: Splice = arr: Exit Function : End If
				k=0 : over = m
				If l>=0 Then
					If l=0 Or n+l-1>=m Then
						over = m
					Else
						over = n+l-1
					End If
					For i=p To n-1
						Redim Preserve a(k)
						If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
						k = k+1
					Next
					If IsArray(elem) Then
						For Each e In elem
							Redim Preserve a(k)
							If IsObject(e) Then Set a(k) = e Else a(k) = e
							k = k+1
						Next
					ElseIf IsNull(elem) Then
						'do nothing.
					Else
						If IsObject(elem) Then Set a(k) = elem Else a(k) = elem
						k = k+1
					End If
					For i=over+1 To m
						Redim Preserve a(k)
						If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
						k = k+1
					Next
				Else
					over = n+l
					If over<p Then over=p
					For i=p To over-1
						Redim Preserve a(k)
						If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
						k = k+1
					Next
					If IsArray(elem) Then
						For Each e In elem
							Redim Preserve a(k)
							If IsObject(e) Then Set a(k) = e Else a(k) = e
							k = k+1
						Next
					ElseIf IsNull(elem) Then
						'do nothing.
					Else
						If IsObject(elem) Then Set a(k) = elem Else a(k) = elem
						k = k+1
					End If
					For i=n+1 To m
						Redim Preserve a(k)
						If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
						k = k+1
					Next
				End If
				tmp = a
				If k=0 Then tmp = Array()
			End If
		Else
			tmp = arr
		End If
		SpliceX = tmp
		On Error GoTo 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Fill(arr, index, elem)
	'# @return: array
	'# @dowhat: 插入元素到数组（填充数组）
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr 		[Array]   : (数组)
	'# @param index 	[Integer] : (索引数字) as index to insert into an array
	'# @param elem	 	[Array | Object|String|Other.. | Null] : 填充元素
	'# 					当 elem参数 为 Array 时, 表示填充批量元素组
	'# 					当 elem参数 为 Object|String|Other.. 时, 表示填充单个元素
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Fill(array("a", "b", "c"), 1, "d") '=> Array("a", "d", "b", "c")
	'# ab.trace ab.a.Fill(array("a", "b", "c"), 5, "d") '=> Array("a", "b", "c", "", "", "d")
	'# ab.trace ab.a.Fill(array("a", "b", "c"), -2, "d") '=> Array("d", "", "a", "b", "c")
	'-------------------------------------------------------------------------------------------

	Public Function Fill(ByVal arr, ByVal index, ByVal elem)
		If Not IsArray(arr) Then : Fill = arr: Exit Function : End If
		If Me.Len(arr)<=0 Then : arr = Array()
		Dim i, k, tmp, a() : k = 0
		If LBound(arr) <= index and UBound(arr) >= index Then
			For i = LBound(arr) To UBound(arr)
				If i < index Then
					Redim Preserve a(k)
					If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
				ElseIf i = index Then
					Redim Preserve a(k)
					If IsObject(elem) Then Set a(k) = elem Else a(k) = elem
					Redim Preserve a(k+1)
					If IsObject(arr(i)) Then Set a(k+1) = arr(i) Else a(k+1) = arr(i)
				Else
					Redim Preserve a(k+1)
					If IsObject(arr(i)) Then Set a(k+1) = arr(i) Else a(k+1) = arr(i)
				End If
				k = k+1
			Next
		ElseIf UBound(arr) < index Then
			For i = LBound(arr) To UBound(arr)
				Redim Preserve a(k)
				If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
				k = k+1
			Next
			For i = UBound(arr)+1 To index
				If i <> index Then
					Redim Preserve a(k)
					a(k) = ""
				Else
					Redim Preserve a(k)
					a(k) = elem
				End If
				k = k+1
			Next
		ElseIf LBound(arr) > index Then
			For i = index To LBound(arr)-1
				If i = index Then
					Redim Preserve a(k)
					If IsObject(elem) Then Set a(k) = elem Else a(k) = elem
				Else
					Redim Preserve a(k)
					a(k) = ""
				End If
				k = k+1
			Next
			For i = LBound(arr) To UBound(arr)
				Redim Preserve a(k)
				If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
				k = k+1
			Next
		End If
		tmp = a
		Fill = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Concat(arr1,arr2)
	'# @return: array
	'# @dowhat: 将两个数组合并成一个数组(新数组)
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr1 [array] : (数组)
	'# @param arr2 [array] : (数组)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Concat(array("a","b","c"), array("c","d")) '=> Array("a","b","c","c","d")
	'# ab.trace ab.a.Concat(array("a","","","b"), array("d","","e")) '=> Array("a","","","b","d","","e")
	'-------------------------------------------------------------------------------------------

	Public Function Concat(ByVal arr1, ByVal arr2)
		Dim tmp, e, i, k, a()
		For Each e in arr1
			Redim Preserve a(k)
			If IsObject(e) Then Set a(k) = e Else a(k) = e
			k = k+1
		Next
		For Each e in arr2
			Redim Preserve a(k)
			If IsObject(e) Then Set a(k) = e Else a(k) = e
			k = k+1
		Next
		tmp = a
		Concat = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Merge(arr1,arr2)
	'# @return: array
	'# @dowhat: 将两个数组合并成一个数组且交叉部分(相同元素部分)只保留一个(新数组)
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr1 [array] : (数组)
	'# @param arr2 [array] : (数组)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Merge(array("a","b","c"), array("c","d")) '=> Array("a","b","c","d")
	'# ab.trace ab.a.Merge(array("a","","","b"), array("d","","e")) '=> Array("a","","","b","d","e")
	'-------------------------------------------------------------------------------------------

	Public Function Merge(ByVal arr1, ByVal arr2)
		Dim tmp, e, i, k, a() : k = 0
		For Each e in arr1
			Redim Preserve a(k)
			If IsObject(e) Then Set a(k) = e Else a(k) = e
			k = k+1
		Next
		For Each e in arr2
			If Not Me.InArray(e,a) Then
				Redim Preserve a(k)
				If IsObject(e) Then Set a(k) = e Else a(k) = e
				k = k+1
			End If
		Next
		tmp = a
		Merge = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Cover(arr1,arr2)
	'# @return: array
	'# @dowhat: 由后面的数组arr2覆盖前面的数组arr1。
	'# 			当arr2数组元素个数不足arr1，保留arr1超过arr2上标部分
	'# 			当arr2数组元素个数超过arr1，函数返回arr2数组
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr1 [array] : (数组)
	'# @param arr2 [array] : (数组)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.cover(array("a","b","c"), array("d","e","f")) '=> Array("d","e","f")
	'# ab.trace ab.a.cover(array("a","b"), array("c","d","e")) '=> Array("c","d","e")
	'# ab.trace ab.a.cover(array("a","b","c"), array("d","e")) '=> Array("d","e","c")
	'-------------------------------------------------------------------------------------------

	Public Function Cover(ByVal arr1, ByVal arr2)
		On Error Resume Next
		Dim e, i, k, a() : k = 0
		If IsArray(arr2) Then
			For Each e In arr2
				Redim Preserve a(k)
				If IsObject(e) Then Set a(k) = e Else a(k) = e
				k = k+1
			Next
		End If
		For i=k To UBound(arr1)
			Redim Preserve a(i)
			If IsObject(arr1(i)) Then Set a(i) = arr1(i) Else a(i) = arr1(i)
		Next
		Cover = a
		On Error Goto 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Remove(arr, p)
	'# @alias: AB.A.Del(arr, p)
	'# @return: array
	'# @dowhat: 按指定值移除数组元素
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : (数组)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.del(array("a", "b", "c", "b"),"b") '=> Array("a", "c")
	'-------------------------------------------------------------------------------------------

	Public Function [Remove](ByVal arr, ByVal p)
		Dim tmp, e, k, a() : k=0
		For Each e in arr
			If IsObject(e) Then
				If Not IsObject(p) Then
					Redim Preserve a(k)
					Set a(k) = e
					k = k+1
				Else
					If Not (e Is p) Then
						Redim Preserve a(k)
						Set a(k) = e
						k = k+1
					End If
				End If
			Else
				If e <> p Then
					Redim Preserve a(k)
					a(k) = e
					k = k+1
				End If
			End If
		Next
		tmp = a
		[Remove] = a
	End Function
	Public Function Del(ByVal arr, ByVal p) : Del = Me.Remove(arr, p) : End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Unset(arr, p)
	'# @return: array
	'# @dowhat: 按指定索引移除数组元素
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : (数组)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.unset(array("a", "b", "c", "b"),1) '=> Array("a", "c", "b")
	'-------------------------------------------------------------------------------------------

	Public Function Unset(ByVal arr, ByVal p)
		Dim tmp, e, i, k, a() : k=0
		For i=LBound(arr) To UBound(arr)
			Redim Preserve a(k)
			If i<>p Then
				If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
				k = k+1
			End If
		Next
		tmp = a
		Unset = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Clean(arr)
	'# @return: array
	'# @dowhat: 移除空元素(净化数组)
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : (数组)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Clean(array("a", "", "", "b")) '=> Array("a", "b")
	'-------------------------------------------------------------------------------------------

	Public Function Clean(ByVal arr)
		Dim tmp, e, k, a() : k=0
		For Each e in arr
			''If Not (IsNull(e) Or e="") Then
			If Not AB.C.IsNul(e) Then
				Redim Preserve a(k)
				If IsObject(e) Then Set a(k) = e Else a(k) = e
				k = k+1
			End If
		Next
		tmp = a
		Clean = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Unique(arr)
	'# @return: array
	'# @dowhat: 移除重复(多余)的元素
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : (数组)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Unique(array("a", "b", "c", "b")) '=> Array("a", "b", "c")
	'-------------------------------------------------------------------------------------------

	Public Function Unique(ByVal arr)
		Dim tmp, e, i, k, a() : k=0
		For Each e in arr
			If Not Me.InArray(e,a) Then
				Redim Preserve a(k)
				If IsObject(e) Then Set a(k) = e Else a(k) = e
				k = k+1
			End If
		Next
		tmp = a
		Unique = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Reverse(arr)
	'# @return: array
	'# @dowhat: 数组元素反向排序
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : (数组)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Reverse(array("a", "b", "c", "d")) '=> Array("d", "c", "b", "a")
	'-------------------------------------------------------------------------------------------

	Public Function Reverse(ByVal arr)
		If Me.Len(arr)<=0 Then : Reverse = Array(): Exit Function : End If
		Dim i, k, tmp, a() : k=0
		For i=UBound(arr) To LBound(arr) Step -1
			Redim Preserve a(k)
			If IsObject(arr(i)) Then Set a(k) = arr(i) Else a(k) = arr(i)
			k = k+1
		Next
		tmp = a
		Reverse = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Range(low, high, step)
	'# @return: array
	'# @dowhat: 建立一个包含指定范围单元的数组, 步进值默认为 1
	'# 			返回数组中从 low 到 high 的单元，包括它们本身;
	'# 			如果 low > high，则序列将从 high 到 low
	'# 			如果 low > high，则序列将从 high 到 low
	'--DESC-------------------------------------------------------------------------------------
	'# @param low [integer] : (数字) 从low开始
	'# @param high [integer] : (数字) 到high结束
	'# @param step [integer] : (数字 或 空值) 作为单元之间的步进值，step 应该为正值。如果未指定(值为空)，则默认为 1
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.range(2, 5, 1) '=> Array(2, 3, 4, 5)
	'# ab.trace ab.a.range(1, 6, 2) '=> Array(1, 3, 5)
	'# ab.trace ab.a.range(6, 1, 2) '=> Array(6, 4, 2)
	'# ab.trace ab.a.range(6, 1, -2) '=> Array(6, 4, 2) '步进值虽为-2 实际上可理解为步退值2
	'# ab.trace ab.a.range("a", "e", 1) '=> Array("a", "b", "c", "d", "e")
	'# ab.trace ab.a.range("F", "A", 2) '=> Array("F","D","B") 即对于有序的ABCDEFG从F倒序到A,步阶为2
	'-------------------------------------------------------------------------------------------

	Public Function Range(ByVal low, ByVal high, ByVal [step])
		If IsNull([step]) Or Trim([step])="" Or [step] = 0 Then [step] = 1
		[step] = CInt([step])
		Dim tmp, i, k, a(), start, over, steps : k = 0 : steps = [step]
		If AB.C.IsInt(low) and AB.C.IsInt(high) Then '0-9
			If low <= high Then
				start = low : over = high : steps = Abs(steps)
			Else
				start = low : over = high : steps = -(Abs(steps))
			End If
		Else 'A-Z 或 a-z
			If (Asc(low)>=65 Or Asc(low)<=90) and (Asc(high)>=65 Or Asc(high)<=90) Or _
				(Asc(low)>=97 Or Asc(low)<=122) and (Asc(high)>=97 Or Asc(high)<=122) Then
				If Asc(low) < Asc(high) Then
					start = Asc(low) : over = Asc(high) : steps = Abs(steps)
				Else
					start = Asc(low) : over = Asc(high) : steps = -(Abs(steps))
				End If
				For i = start To over Step steps
					Redim Preserve a(k)
					a(k) = Chr(i)
					k = k + 1
				Next
				Range = a
				Exit Function
			End If
		End If
		For i = start To over Step steps
			Redim Preserve a(k)
			a(k) = i
			k = k + 1
		Next
		tmp = a
		Range = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Rand(arr, num)
	'# @return: array
	'# @dowhat: 对数组进行乱序组合新的数组，可定义新数组元素个数
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] 	: (数组)
	'# @param arr [integer] : (整数) as specifies how many entries you want to pick
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Rand(array("a", "b", "c"), 5) '=> Array("a", "a", "c", "a", "c")
	'-------------------------------------------------------------------------------------------

	Public Function Rand(ByVal arr, ByVal num)
		If Me.Len(arr)<=0 Then : Rand = Array(): Exit Function : End If
		Dim tmpi, tmp, i, a()
		For i = 0 To num -1
			Randomize
			tmpi = Int((UBound(arr) + 1) * Rnd)
			Redim Preserve a(i)
			If IsObject(arr(tmpi)) Then Set a(i) = arr(tmpi) Else a(i) = arr(tmpi)
		Next
		tmp = a
		Rand = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Sort(arr)
	'# @return: array
	'# @dowhat: 按顺序(从小到大)进行重组数组 (所有数组元素均须为数字类型)
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Sort(array(2, 1, 3)) '=> Array(1, 2, 3)
	'# ab.trace ab.a.Sort(array(1, 7, "a", "d", "c", 4)) '=> Array("d", "c", "a", 7, 4, 1)
	'-------------------------------------------------------------------------------------------

	Public Function Sort(ByVal arr)
		On Error Resume Next
		Dim i, j, f, a, b : a = Array() : b = Array()
		' 纯数字可用下面算法，但含字符串就不可取：
		' ReDim a(UBound(arr))
		' For i = 0 To UBound(a)
			' a(i) = CDbl(arr(i))
		' Next
		' For i = 0 To UBound(a)
			' For j = i + 1 To UBound(a)
				' If a(i) > a(j) Then
					' f = a(i)
					' a(i) = a(j)
					' a(j) = f
				' End If
			' Next
		' Next
		If Not AB.C.IsNul(arr) Then
			For i = 0 to UBound(arr)
				If IsObject(arr(i)) Or IsArray(arr(i)) Then
					b = Me.Push( b, arr(i) )
				Else
					a = Me.Push( a, arr(i) )
				End If
			Next
			Dim check : check = True
			Do Until check = False
				check = False
				For i = 0 to UBound(a)-1
					If a(i) > a(i+1) Then
						f = a(i)
						a(i) = a(i+1)
						a(i+1) = f
						check = True
					End If
				Next
			Loop
		End If
		a = Me.Merge( a, b )
		Sort = a
		On Error Goto 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.rSort(arr)
	'# @return: array
	'# @dowhat: 按倒序(从大到小)进行重组数组 (所有数组元素均须为数字类型)
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.rSort(array(2, 1, 3)) '=> Array(3, 2, 1)
	'# ab.trace ab.a.rSort(array(1, 7, "a", "d", "c", 4)) '=> Array(1, 4, 7, "a", "c", "d")
	'-------------------------------------------------------------------------------------------

	Public Function rSort(ByVal arr)
		On Error Resume Next
		Dim i, j, f, a, b : a = Array() : b = Array()
		' 纯数字可用下面算法，但含字符串就不可取：
		' ReDim a(UBound(arr))
		' For i = 0 To UBound(a)
			' a(i) = CDbl(arr(i))
		' Next
		' For i = 0 To UBound(a)
			' For j = i + 1 To UBound(a)
				' If a(i) < a(j) Then
					' f = a(i)
					' a(i) = a(j)
					' a(j) = f
				' End If
			' Next
		' Next
		If Not AB.C.IsNul(arr) Then
			For i = 0 to UBound(arr)
				If IsObject(arr(i)) Or IsArray(arr(i)) Then
					b = Me.Push( b, arr(i) )
				Else
					a = Me.Push( a, arr(i) )
				End If
			Next
			Dim check : check = True
			Do Until check = False
				check = False
				For i = 0 to UBound(a)-1
					If a(i) < a(i+1) Then
						f = a(i)
						a(i) = a(i+1)
						a(i+1) = f
						check = True
					End If
				Next
			Loop
		End If
		a = Me.Merge( a, b )
		rSort = a
		On Error Goto 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Shuffle(arr)
	'# @return: array
	'# @dowhat: 对数组进行随机排序
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Shuffle(array(2, 1, 3)) '=> Array(1, 3, 2)
	'-------------------------------------------------------------------------------------------

	Public Function Shuffle(ByVal arr)
		Dim m, n, i
		'i = Search(arr,Rand(arr,1))
		'arr = Splice(arr,i,i+1)
		Randomize
		For i = 0 to UBound(arr)
			m = Int(Rnd()*i)
			n = arr(m)
			If IsObject(arr(i)) Then Set arr(m) = arr(i) Else arr(m) = arr(i)
			If IsObject(n) Then Set arr(i) = n Else arr(i) = n
		Next
		Shuffle = arr
	End Function

	'---------------------------------------------------------------------
	'# AB.A.Split(sText,sSplit)
	'# @return: integer
	'# @dowhat: 字符串分割为数组,此方法是原生Split函数的补充
	'# 			当分割字符sSplit值为空时，返回以每个字符组成的数组
	'--DESC---------------------------------------------------------------
	'# @param sText [string] : 原字符串
	'# @param sSplit [string / Integer] : 分割字符
	'--DEMO---------------------------------------------------------------
	'# AB.Trace AB.A.Split("a,b,c",",") '返回：Array("a","b","c")
	'# AB.Trace AB.A.Split("abcd","") '返回：Array("a","b","c","d")
	'# AB.Trace AB.A.Split("abcdefg",3) '返回：Array("abc","def","g")
	'# AB.Trace AB.A.Split("abcd",0) '返回：Array("a","b","c","d")
	'#--------------------------------------------------------------------

	Function Split(Byval sText, Byval sSplit)
		On Error Resume Next
		Split = AB.C.Split(sText,sSplit)
		On Error GoTo 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.toArr(sText,sSplit)
	'# @return: integer
	'# @dowhat: 字符串分割为数组,此方法是原Split函数的补充
	'# 			当分割字符sSplit值为空时，返回以每个字符组成的数组
	'# @author: Lajox; version: 1.0.0 (2012-04-21)
	'--DESC-------------------------------------------------------------------------------------
	'# @param sText [string] : 原字符串
	'# @param sSplit [string] : 分割字符
	'--DEMO-------------------------------------------------------------------------------------
	'# AB.Trace AB.A.toArr("a,b,c",",") '返回：Array("a","b","c")
	'# AB.Trace AB.A.toArr("abcd","") '返回：Array("a","b","c","d")
	'--------------------------------------------------------------------------------------------

	Function toArr(Byval sText, Byval sSplit)
		Dim temp
		temp = Me.Split(sText,sSplit)
		toArr = temp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.toStr(a, s)
	'# @return: string
	'# @dowhat: 将数组转为字符串
	'# 			数组各个元素按某分隔符拼接成字符串，可代替Join函数
	'--DESC-------------------------------------------------------------------------------------
	'# @param a [array] : 数组
	'# @param s [string] : 分隔符
	'--DEMO-------------------------------------------------------------------------------------
	'# AB.C.Print AB.A.toStr(Array("a","b","c"), "|") '返回：a|b|c
	'--------------------------------------------------------------------------------------------

	Public Function toStr(Byval a, Byval s)
		Dim str,i
		If Not IsArray(a) Then Exit Function
		'For i=0 To Ubound(a)
			'If i=0 Then
				'str = str & a(i)
			'Else
				'str = str & s & a(i)
			'End If
		'Next
		If IsArray(a) Then str = Join(a, s) Else str = a
		toStr = str
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.ToString(a)
	'# @return: string
	'# @dowhat: 数组各个元素按分隔符(,)拼接成字符串
	'--DESC-------------------------------------------------------------------------------------
	'# @param a : [array] 数组
	'--DEMO-------------------------------------------------------------------------------------
	'# AB.C.Print AB.A.ToString(Array("a","b","c")) '返回：a,b,c
	'--------------------------------------------------------------------------------------------

	Public Function ToString(ByVal arr)
		If IsArray(arr) Then
			Dim tmp
			tmp = Join(arr, ",")
			ToString = tmp
		Else
			ToString = arr
		End If
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.ToArray(s)
	'# @return: array
	'# @dowhat: 字符串转数组, 将字符串按某分隔符(,)分割为数组
	'--DESC-------------------------------------------------------------------------------------
	'# @param s [string] : 字符串 当s为空时,返回上标-1的动态数组
	'--DEMO-------------------------------------------------------------------------------------
	'# Dim s : s = "a,b,c"
	'# Dim a : a = AB.A.ToArray(s)
	'# If UBound(a)>-1 Then ab.trace(a)
	'--------------------------------------------------------------------------------------------

	Public Function ToArray(ByVal s)
		Dim str : str = s
		If IsNull(str) Then str = ""
		Dim a : a = Me.Split(str, ",")
		Dim i, k, b(), c
		If Not IsNull(str) and str<>"" Then
			k = 0
			For i=0 To Ubound(a)
				Redim Preserve b(k)
				b(k) = Trim(a(i))
				k = k + 1
			Next
			c = b
		Else
			c = a
		End If
		ToArray = c
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.InArray(s, arr)
	'# @return: boolean
	'# @dowhat: 判断一个值是否存在于数组，若非数组则返回False
	'--DESC-------------------------------------------------------------------------------------
	'# @param s   [string] : 某项值
	'# @param arr [array] : 数组
	'--DEMO-------------------------------------------------------------------------------------
	'# AB.C.Print AB.A.InArray("2", Array("1","2","3")) '输出：True
	'-------------------------------------------------------------------------------------------

	Public Function InArray(Byval s, Byval arr)
		On Error Resume Next
		If Not IsArray(arr) Then InArray = False : Exit Function
		' Dim I
		' InArray = False
		' For I = LBound(arr) To UBound(arr)
			' If Not IsObject(arr(I)) and Not IsObject(s) Then
				' If arr(I) = s Then
					' InArray = True
					' Exit For
				' End If
			' ElseIf IsObject(arr(I)) and IsObject(s) Then
				' If TypeName(arr(I)) = TypeName(s) and VarType(arr(I)) = VarType(s) Then
					' InArray = True
					' Exit For
				' End If
			' End If
		' Next
		Dim x
		InArray = False
		For Each x In arr
			If Not IsObject(x) and Not IsObject(s) Then
				If IsArray(x) Or IsArray(s) Then
					If TypeName(x) = TypeName(s) and VarType(x) = VarType(s) Then
						If Size(x) = Size(s) Then
							If x = s Then
								InArray = True
								Exit For
							End If
						End If
					End If
				Else
					If x = s Then
						InArray = True
						Exit For
					End If
				End If
			ElseIf IsObject(x) and IsObject(s) Then
				If TypeName(x) = TypeName(s) and VarType(x) = VarType(s) Then
					InArray = True
					Exit For
				End If
			End If
		Next
		On Error Goto 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Count(arr, s)
	'# @return: integer
	'# @dowhat: 检索(一维)数组里含有多少个某项值
	'# @author: Lajox; version: 1.0.0 (2012-04-21)
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'# @param s   [string] : 某项值
	'--DEMO-------------------------------------------------------------------------------------
	'# AB.C.Print AB.A.Count(Array("a","b","b","c"),"b") '输出：2
	'-------------------------------------------------------------------------------------------

	Function Count(arr, s)
		On Error Resume Next
		If Not IsArray(arr) Then:Count=0: Exit Function: End If '非数组返回0
		Dim I,iCount:iCount=0
		'AB.Use "H":If AB.H.Len(s) > 0 Then iCount=(AB.H.Len(Join(arr,""))-AB.H.Len(Replace(Join(arr,""),s,"")))/AB.H.Len(s)
		For I=LBound(arr) To UBound(arr)
			If Not IsObject(arr(I)) and Not IsObject(s) Then
				If s = arr(I) Then iCount=iCount+1
			ElseIf IsObject(arr(I)) and IsObject(s) Then
				If TypeName(arr(I)) = TypeName(s) and VarType(arr(I)) = VarType(s) Then
					iCount=iCount+1
				End If
			End If
		Next
		Count = iCount
		On Error GoTo 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Search(arr, value)
	'# @return: integer
	'# @dowhat: 查询元素是否存在并返回该值对应的索引
	'# 			查询元素，不存在则返回-1, 存在则返回该值对应的索引
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr     [array] 	 : (数组)
	'# @param value   [anything] : Anything (任意值) Searching value
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.c.print ab.a.Search(array("a", "b", "c"), "b") '=> 1
	'-------------------------------------------------------------------------------------------

	Public Function Search(ByVal arr, ByVal Value)
		If Me.Len(arr)<=0 Then : Search = -1: Exit Function : End If
		Dim i
		For i = 0 To UBound(arr)
			If Not IsObject(arr(i)) and Not IsObject(Value) Then
				If arr(i) = Value Then
					Search = i
					Exit Function
				End If
			ElseIf IsObject(arr(i)) and IsObject(Value) Then
				If TypeName(arr(i)) = TypeName(Value) and VarType(arr(i)) = VarType(Value) Then
					Search = i
					Exit Function
				End If
			End If
		Next
		Search = -1
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Index(arr, s)
	'# @return: integer
	'# @dowhat: 检测字符串元素在(一维)数组中的索引(Index)
	'# 			如果为非数组则返回值(-1), 如果数组中找不到该元素也返回值(-1)
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'# @param s   [string] : 字符串元素
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.c.print ab.a.Index(array("ab","cd","efg"),"cd") '输出：1
	'-------------------------------------------------------------------------------------------

	Public Function Index(Byval arr, Byval s)
		On Error Resume Next
		If Not IsArray(arr) Then:Index=-1: Exit Function: End If
		Dim i,temp:temp = -1
		For I=LBound(arr) To UBound(arr)
			If Not IsObject(arr(I)) and Not IsObject(s) Then
				If s = arr(I) Then
					temp = I
					Exit For
				End If
			ElseIf IsObject(arr(I)) and IsObject(s) Then
				If TypeName(arr(I)) = TypeName(s) and VarType(arr(I)) = VarType(s) Then
					temp = I
					Exit For
				End If
			End If
		Next
		Index = temp
		On Error GoTo 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Len(arr)
	'# @return: integer
	'# @dowhat: 检测(一维)数组长度(元素个数)
	'# 			检测(一维)数组长度大小, 非数组返回值(-1), 动态数组返回值(0)
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'--DEMO-------------------------------------------------------------------------------------
	'# dim a,b(),i
	'# a = array() : ab.c.printcn ab.a.len(a) '输出：0
	'# a = array("a","b") : ab.c.printcn ab.a.len(a) '输出：2
	'# a = split("", ",") : ab.c.printcn ab.a.len(a) '输出：0
	'# ab.c.printcn ab.a.len(b) '输出：0
	'# redim preserve b(3) : b(3) = "x" : ab.c.printcn ab.a.len(b) '输出：4
	'# redim preserve b(0) : b(0) = "t" : ab.c.printcn ab.a.len(b) '输出：1
	'# a = array("a","b","c")
	'# if ab.a.len(a)>0 then
	'#   for i=0 to ab.a.len(a)-1
	'# 	   ab.c.print "<br>" & a(i)
	'#   next
	'# end if
	'-------------------------------------------------------------------------------------------

	Public Function [Len](Byval arr)
		On Error Resume Next
		If Not IsArray(arr) Then:[Len]=-1:End If
		Dim temp:temp=Ubound(arr)
		If Err Or temp<0 Then:[Len]=0:Err.Clear:Exit Function:End If
		Dim i,iCount:iCount=0
		For i=Lbound(arr) To Ubound(arr)
			iCount = iCount + 1
		Next
		[Len] = iCount
		On Error GoTo 0
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Size(arr)
	'# @return: integer
	'# @dowhat: 检测数组维数, 非数组返回值(-1), 动态数组返回值(0)
	'# 			未初始化的动态数组、或Split函数分割返回的数组上标Ubound值-1, 则返回: 0
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr [array] : 数组
	'--DEMO-------------------------------------------------------------------------------------
	'# dim a(2,3) : ab.c.printcn ab.a.size(a) '输出：2
	'# dim b : b = split("", ",") : ab.c.printcn ab.a.size(b) '输出：0
	'# dim c(2,2)
	'# c(0,0) = "0-0" : c(0,1) = "0-1" : c(0,2) = "0-2"
	'# c(1,0) = "1-0" : c(1,1) = "1-1" : c(1,2) = "1-2"
	'# c(2,0) = "2-0" : c(2,1) = "2-1" : c(2,2) = "2-2"
	'# dim d(2) : d(0) = "0" : d(1) = "1" : d(2) = "2"
	'# dim e(4,5,8)
	'# ab.c.printcn ab.a.size(c) & "-" & ab.a.size(d) & "-" & ab.a.size(e) '输出：2-1-3
	'# ab.trace c
	'-------------------------------------------------------------------------------------------

	Public Function Size(Byval arr)
		On Error Resume Next
		If Not IsArray(arr) Then:Size=-1:Exit Function:End If
		Dim temp:temp=Ubound(arr)
		If Err Or temp<0 Then:Size=0:Err.Clear:Exit Function:End If
		Dim m : m = 0 '限制最大检测维数(值为 0 则不限制)
		'If m = 0 Then m = 30
		Dim n,i,k:k=0
		Err.Clear
		If m <= 0 Then '无限循环法
			Dim tm
			Do While Err.Number = 0
				k = k + 1
				tm = Ubound(arr,k)
			Loop
			If Err Then k = k - 1 : Err.Clear
			Size = k
		ElseIf m > 0 Then '维数限定法
			For i=1 To m
				Dim tn : tn = Ubound(arr,i)
				If Err then
					Err.Clear
					Exit For
				Else
					k = k + 1
				End if
			Next
			Size = k
		End If
		On Error GoTo 0
	End Function

	'------------------------------------------------------------------------------------------
	'# AB.A.Chunk(arr, size)
	'# @return: Integer / Double
	'# @dowhat: 将一个(一维)数组分割成多个数组，其中每个数组的单元数目由 size 决定
	'# 			最后一个数组的单元数目可能会少几个
	'--DESC------------------------------------------------------------------------------------
	'# @param arr: [array] (数组) 原数组
	'# @param size: [integer] (整型) 每个数组的单元数目(一般 size >= 1) 当 size <=0 则值返回原数组
	'--DEMO------------------------------------------------------------------------------------
	'# Dim a : a = AB.A.Chunk(array(1,2,3,4,5,6,7), 3)
	'# AB.Trace a '输出：Array(array(1,2,3), array(4,5,6), array(7))
	'# If AB.A.Len(a)>0 Then AB.Trace a(0) '输出： Array(1,2,3)
	'# If AB.A.Len(a)>1 Then AB.Trace a(1) '输出： Array(4,5,6)
	'# If AB.A.Len(a)>2 Then AB.Trace a(2) '输出： Array(7)
	'# Dim i, j
	'# For i = 0 To Ubound(a)
	'# 	AB.C.PrintCn "a(" & i & ") =>"
	'# 	For j = 0 To Ubound(a(i))
	'# 		AB.C.PrintCn a(i)(j)
	'# 	Next
	'# Next
	'------------------------------------------------------------------------------------------

	Public Function Chunk(ByVal arr, Byval size)
		Dim tmp : tmp = Array()
		If IsArray(arr) and Me.Len(arr) > 0 Then
			size = CInt(size) : If CInt(size) <= 0 Then : Chunk = arr : Exit Function : End If
			Dim i, j, k, n, l : k = 0 : l = Me.Len(arr)
			If (Me.Len(arr) Mod size) = 0 Then
				n = l \ size
			Else
				n = l \ size + 1
			End If
			For i = 0 To n-1
				Redim Preserve tmp(i)
				tmp(i) = Array()
				For j = 0 To size-1
					If k < l Then tmp(i) = Me.Push(tmp(i), arr((i)*size + (j+1) - 1))
					k = k + 1
				Next
			Next
		End If
		Chunk = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Diff(arr1,arr2)
	'# @return: array
	'# @dowhat: 返回一个数组，该数组包括了所有在array1数组中但是不在array2数组中的项值
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr1 [array] : (数组1)
	'# @param arr2 [array] : (数组2)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Diff(array("a","b","c"), array("a","d","c")) '=> Array("b")
	'# ab.trace ab.a.Diff(array("a","e","e","f"), array("x","e","y")) '=> Array("a","f")
	'-------------------------------------------------------------------------------------------

	Public Function Diff(ByVal arr1, ByVal arr2)
		If Not IsArray(arr1) Or Not IsArray(arr2) Then : Diff = Array() : Exit Function : End If
		Dim tmp, e, i, k, a() : k = 0
		For Each e in arr1
			If Not Me.InArray(e, arr2) Then
				Redim Preserve a(k)
				If IsObject(e) Then Set a(k) = e Else a(k) = e
				k = k + 1
			End If
		Next
		tmp = a
		Diff = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Same(arr1,arr2)
	'# @return: array
	'# @dowhat: 返回一个数组，该数组包括了既在array1数组中又在array2数组中的项值
	'--DESC-------------------------------------------------------------------------------------
	'# @param arr1 [array] : (数组1)
	'# @param arr2 [array] : (数组2)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.Same(array("a","b","c"), array("a","d","c")) '=> Array("a","c")
	'# ab.trace ab.a.Same(array("a","e","e","f","z"), array("x","e","y","z")) '=> Array("e","z")
	'-------------------------------------------------------------------------------------------

	Public Function Same(ByVal arr1, ByVal arr2)
		If Not IsArray(arr1) Or Not IsArray(arr2) Then : Diff = Array() : Exit Function : End If
		Dim tmp, e, i, k, a() : k = 0
		For Each e in arr1
			If Me.InArray(e, arr2) And Not Me.InArray(e, a) Then
				Redim Preserve a(k)
				If IsObject(e) Then Set a(k) = e Else a(k) = e
				k = k + 1
			End If
		Next
		tmp = a
		Same = tmp
	End Function

	'-------------------------------------------------------------------------------------------
	'# AB.A.Fetch(s, p)
	'# @return: array
	'# @dowhat: 字符串转为数组，且该数组所有子项不为空
	'--DESC-------------------------------------------------------------------------------------
	'# @param s [string] : (原字符串)
	'# @param p [string] : (分隔符)
	'--DEMO-------------------------------------------------------------------------------------
	'# ab.trace ab.a.fetch("1,,,,3,,5,", ",") '输出 Array(1,3,5)
	'-------------------------------------------------------------------------------------------

	Public Function Fetch(ByVal s, ByVal p)
		Dim t, e, a, i : a = Array()
		s = Trim(s)
		If p="" Then p = ","
		If s<>"" Then
			t = Split(s, p)
			For i=0 To UBound(t)
				e = Trim(t(i))
				If e<>"" Then
					If AB.C.IsNum(e) Then e = CLng(e)
					a = Me.Push(a, e)
				End If
			Next
		End If
		Fetch = a
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.A.CheckArray(a1, a2, p)
	'@ 返  回:  True/False
	'@ 作  用:  比较两个(一维)数组,当p=0初步比较两个数组上标和下标是否一致,当p=1比较两个数组是否完全相等
	'==DESC=====================================================================================
	'@ 参数 a1 : [Array]
	'@ 参数 a2 : [Array]
	'@ 参数 p : 0 Or 1
	'==DEMO=====================================================================================
	'@ AB.C.PrintCn AB.A.CheckArray(Array(1,2,3), Array(1,2,4), 0) '=> True
	'@ AB.C.PrintCn AB.A.CheckArray(Array(1,2,3), Array(1,2,4), 1) '=> False
	'@ AB.C.PrintCn AB.A.CheckArray("a", "a", 0) '=> False
	'@ *****************************************************************************************

	Public Function CheckArray(Byval a1, Byval a2, Byval p)
		On Error Resume Next
		Dim Result:Result = True
		IF IsArray(a1) And IsArray(a2) Then
			IF UBound(a1)<>UBound(a2) Or LBound(a1)<>LBound(a2) Then
				Result = False
			Else
				If p=1 Then
					Dim I
					For I=LBound(a1) To UBound(a1)
						If Not IsObject(a1(I)) and Not IsObject(a2(I)) Then
							If VarType(a1(I))<>VarType(a2(I)) Or a1(I)<>a2(I) Then
								Result = False
								Exit For
							End If
						ElseIf IsObject(a1(I)) and IsObject(a2(I)) Then
							If TypeName(a1(I)) <> TypeName(a2(I)) Or VarType(a1(I)) <> VarType(a2(I)) Then
								Result = False
								Exit For
							End If
						End If
					Next
				ElseIf p=0 Then
					Result = True
				End If
			End IF
		Else
			Result = False
		End IF
		CheckArray = Result
		On Error GoTo 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.A.SetArray(Arr, By, str) 按数组形式匹配填充设置数组
	'@ 返  回:  [Array]
	'@ 作  用:  数组Arr按指定特定数组(一维)元素个数或指定值个数进行匹配填充，不足个数按oStr赋值添加剩余数组子项，
	'@			若Arr元素个数超过By,则多出的部分则不要
	'==DESC=====================================================================================
	'@ 参数 Arr :	[Array]  数组
	'@ 参数 By  :	[Array 或 Integer]  指定特定数组(一维) 或 指定值数
	'@ 参数 str :	[String / Integer]  填充字符
	'==DEMO=====================================================================================
	'@ AB.Trace AB.A.SetArray(Array("a","b"), Array("e","f","g"), "c") 	'=> Array("a","b","c")
	'@ AB.Trace AB.A.SetArray(Array("a","b"), Array("e"), "c") 			'=> Array("a")
	'@ AB.Trace AB.A.SetArray(Array("a","b"), 1, "c") 					'=> Array("a")
	'@ *****************************************************************************************

	Public Function SetArray(Byval Arr,Byval By,Byval str)
		IF Err Then Err.Clear
		On Error Resume Next
		IF IsNull(str) Or Trim(str)="" Then str=""
		Dim temp,A(),I
		Dim ai : ai = 0 : IF IsArray(By) Then ai = UBound(By) Else ai = CLng(By)-1
		IF IsArray(Arr) Then 'arrBy和arr都是数组
			IF ai<=UBound(Arr) Then '当数组arrBy不大于arr数组时，对应匹配替换
				For I=0 To ai
					Redim Preserve A(I)
					A(I) = Arr(I)
				Next
			Else '当数组arrBy大于arr数组时，前面部分对应匹配，arrBy超过arr的部分则以赋值oStr新增元素
				For I=0 To UBound(Arr)
					Redim Preserve A(I)
					If IsObject(Arr(I)) Then Set A(I) = Arr(I) Else A(I) = Arr(I)
				Next
				For I=UBound(Arr)+1 To ai
					Redim Preserve A(I)
					If IsObject(str) Then Set A(I) = str Else A(I) = str
				Next
			End IF
			temp = A
		Else
			temp = Arr
		End IF
		SetArray = temp
		On Error GoTo 0
	End Function

End Class
%>