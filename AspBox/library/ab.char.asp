<%
'######################################################################
'## ab.char.asp
'## -------------------------------------------------------------------
'## Feature     :   AspBox Char Block
'## Version     :   v1.0.1
'## Author      :   Lajox(lajox@19www.com)
'## Update Date :   2014/12/11 21:34
'## Description :   AspBox Char Block
'######################################################################

Class Cls_AB_Char

	Private b_init, m_lPower2(31)

	Private Sub Class_Initialize()
		b_init = False
		AB.Use "H"
		Init()
	End Sub

	Private Sub Class_Terminate()

	End Sub

	Private Sub Init()
		On Error Resume Next
		m_lPower2(0) = &H1&
		m_lPower2(1) = &H2&
		m_lPower2(2) = &H4&
		m_lPower2(3) = &H8&
		m_lPower2(4) = &H10&
		m_lPower2(5) = &H20&
		m_lPower2(6) = &H40&
		m_lPower2(7) = &H80&
		m_lPower2(8) = &H100&
		m_lPower2(9) = &H200&
		m_lPower2(10) = &H400&
		m_lPower2(11) = &H800&
		m_lPower2(12) = &H1000&
		m_lPower2(13) = &H2000&
		m_lPower2(14) = &H4000&
		m_lPower2(15) = &H8000&
		m_lPower2(16) = &H10000
		m_lPower2(17) = &H20000
		m_lPower2(18) = &H40000
		m_lPower2(19) = &H80000
		m_lPower2(20) = &H100000
		m_lPower2(21) = &H200000
		m_lPower2(22) = &H400000
		m_lPower2(23) = &H800000
		m_lPower2(24) = &H1000000
		m_lPower2(25) = &H2000000
		m_lPower2(26) = &H4000000
		m_lPower2(27) = &H8000000
		m_lPower2(28) = &H10000000
		m_lPower2(29) = &H20000000
		m_lPower2(30) = &H40000000
		m_lPower2(31) = &H80000000
		b_init = True 'Success Init()
		On Error Goto 0
	End Sub

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.Trim(string, charlist)
	'@ 返  回:  String (字符串)
	'@ 作  用:  去除字符串首尾空格
	'==DESC=====================================================================================
	'@ 参数 string : String (字符串) 要检查的字符串
	'@ 参数 charlist : String (字符串) 可选, 除了删除了首尾空格之后还要删除这些首尾字符串。
	'==DEMO=====================================================================================
	'@ AB.Use "Char"
	'@ AB.C.PrintCn AB.Char.Trim(" abcd efg ", "") '返回字符串: "abcd efg"
	'@ AB.C.PrintCn AB.Char.Trim(" abcd efg ", "ab") '返回字符串: "cd efg"
	'@ AB.C.PrintCn AB.Char.Trim(" abcd efg ", Array("a","b","g")) '返回字符串: "bcd ef"
	'@ *****************************************************************************************

	Public Function [Trim](ByVal s, ByVal c)
		On Error Resume Next
		If s <> "" Then
			Dim temp, e, cs, i : i = 0
			temp = AB.C.RegReplace(s, "(^\s*)|(\s*$)", "")
			If IsArray(c) Then
				For Each e In c
					If Not (IsArray(e) Or IsObject(e)) Then
						If i = 0 Then cs = "(^"&e&"*)|("&e&"*$)" Else cs = cs & "|(^"&e&"*)|("&e&"*$)"
					End If
					i = i + 1
				Next
				If cs <> "" Then temp = AB.C.RegReplace(temp, cs, "")
			ElseIf AB.H.Trim(c&"") <> "" Then
				temp = AB.C.RegReplace(temp, "(^"&c&"*)|("&c&"*$)", "")
			End If
			[Trim] = temp
		End If
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.LTrim(string, charlist)
	'@ 返  回:  String (字符串)
	'@ 作  用:  去除字符串首空格
	'==DESC=====================================================================================
	'@ 参数 string : String (字符串) 要检查的字符串
	'@ 参数 charlist : String (字符串) 可选, 除了删除了首空格之后还要删除这些首字符串。
	'==DEMO=====================================================================================
	'@ AB.Use "Char"
	'@ AB.C.PrintCn AB.Char.LTrim(" abcd efg ", "") '返回字符串: "abcd efg "
	'@ AB.C.PrintCn AB.Char.LTrim(" abcd efg ", "ab") '返回字符串: "cd efg "
	'@ AB.C.PrintCn AB.Char.LTrim(" abcd efg ", Array("a","b","g")) '返回字符串: "bcd ef "
	'@ *****************************************************************************************

	Public Function [LTrim](ByVal s, ByVal c)
		On Error Resume Next
		If s <> "" Then
			Dim temp, e, cs, i : i = 0
			temp = AB.C.RegReplace(s, "(^\s*)", "")
			AB.Use "H"
			If IsArray(c) Then
				For Each e In c
					If Not (IsArray(e) Or IsObject(e)) Then
						If i = 0 Then cs = "(^"&e&"*)" Else cs = cs & "|(^"&e&"*)"
					End If
					i = i + 1
				Next
				If cs <> "" Then temp = AB.C.RegReplace(temp, cs, "")
			ElseIf AB.H.LTrim(c&"") <> "" Then
				temp = AB.C.RegReplace(temp, "(^"&c&"*)", "")
			End If
			[LTrim] = temp
		End If
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.RTrim(string, charlist)
	'@ 返  回:  String (字符串)
	'@ 作  用:  去除字符串尾空格
	'==DESC=====================================================================================
	'@ 参数 string : String (字符串) 要检查的字符串
	'@ 参数 charlist : String (字符串) 可选, 除了删除了尾空格之后还要删除这些尾字符串。
	'==DEMO=====================================================================================
	'@ AB.Use "Char"
	'@ AB.C.PrintCn AB.Char.RTrim(" abcd efg ", "") '返回字符串: " abcd efg"
	'@ AB.C.PrintCn AB.Char.RTrim(" abcd efg ", "g") '返回字符串: " abcd ef"
	'@ AB.C.PrintCn AB.Char.RTrim(" abcd efg ", Array("a","b","g")) '返回字符串: " abcd ef"
	'@ *****************************************************************************************

	Public Function [RTrim](ByVal s, ByVal c)
		On Error Resume Next
		If s <> "" Then
			Dim temp, e, cs, i : i = 0
			temp = AB.C.RegReplace(s, "(\s*$)", "")
			AB.Use "H"
			If IsArray(c) Then
				For Each e In c
					If Not (IsArray(e) Or IsObject(e)) Then
						If i = 0 Then cs = "("&e&"*$)" Else cs = cs & "|("&e&"*$)"
					End If
					i = i + 1
				Next
				If cs <> "" Then temp = AB.C.RegReplace(temp, cs, "")
			ElseIf AB.H.RTrim(c&"") <> "" Then
				temp = AB.C.RegReplace(temp, "("&c&"*$)", "")
			End If
			[RTrim] = temp
		End If
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.strLen(s)
	'@ 返  回:  字符串真实长度(字符宽度)
	'@ 作  用:  计算字符串宽度（汉字则宽度为2, 英文字母及数字为1）
	'==DESC=====================================================================================
	'@ 参数 s : 字符串
	'@ asc(s) => 0~32 : 控制字符或通讯专用字符
	'@ asc(s) => 33~47 : 标点符号 ! " # $ % & ' ( ) * + , - . /
	'@ asc(s) => 48~57 : 数字 0123456789
	'@ asc(s) => 58~64 : 标点符号 : ; < = > ? @
	'@ asc(s) => 65~90 : 大写英文字母 ABCDEFGHIJKLMNOPQRSTUVWXYZ
	'@ asc(s) => 91~96 : 标点符号 [ \ ] ^ _ `
	'@ asc(s) => 97~122 : 小写英文字母 abcdefghijklmcopqrstuvwxyz
	'@ asc(s) => 123~126 : 标点符号 { } ~
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Char.strLen("abcdefg中文字符") '返回: 15
	'@ *****************************************************************************************

	Public Function strLen(Byval s)
		Dim alg : alg=1 '指定算法
		Dim i, k, x, c
		If alg=1 Then
			If IsNull(s) Or s="" Then
				strLen = 0
			Else
				k = 0
				For i = 1 To Len(s)
					c = Mid(s, i, 1)
					'''If Abs(Ascw(c))<=255 Then
					If AB.C.strAsc(c)>=0 and AB.C.strAsc(c)<=255 Then
						k = k + 1
					Else
						k = k + 2
					End If
				Next
				strLen = k
			End If
		ElseIF alg=2 Then
			i = 0
			If s<>"" Then
				For x=1 To Len(s)
					i = i + Len(Hex(AB.C.strAsc(Mid(s, x, 1))))/2 '是中文就长度加1
				Next
			End If
			strLen = i
		ElseIF alg=3 Then
			If isNull(s) Or s="" Then:strLen=0:Exit Function:End If
			Dim WINNT_CHINESE
			WINNT_CHINESE=(Len("例子")=2)
			If WINNT_CHINESE Then
				k = Len(s)
				For i=1 To Len(s)
					c = AB.C.strAsc(Mid(s,i,1))
					If c<0 Then c = c + 65536
					If c>255 Then k = k + 1
				Next
				strLen = k
			Else
				strLen = Len(s)
			End If
		Else
			k = 0
			Dim Rep
			Set Rep = New RegExp
			Rep.Global = True
			Rep.IgnoreCase = True
			Rep.Pattern = "[\u4E00-\u9FA5\uF900-\uFA2D]"
			For Each i In Rep.Execute(s)
				k = k + 1
			Next
			Set Rep=Nothing
			k = k + Len(s)
			strLen = k
		End IF
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.strSub(str, n, p)
	'@ 返  回:  String (字符串)
	'@ 作  用:  按字符宽度截取字符串
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'@ 参数 n   : String (字符串) 截取的字符宽度
	'@ 参数 p   : String (字符串) 后缀修饰符
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Char.strSub("abcdefg中文字符", 8, "..") '返回字符串: "abcdefg.."
	'@ *****************************************************************************************

	Public Function strSub(Byval s, Byval n, Byval p)
		Dim p_num, x, i, k, c
		If IsNull(s) Or AB.H.Trim(s)="" Then
			strSub = ""
		Else
			If n<=0 Or strLen(s)<=n Then
				strSub = s
			Else
				p_num = 0
				x = 0
				Do While Not p_num > n-1
					x = x + 1
					k = x
					c = AB.C.strAsc(Mid(s,x,1))
					If c<0 Then c = c + 65536
					If c>255 Then
						p_num = Int(p_num) + 2
					Else
						p_num = Int(p_num) + 1
					End If
					IF p_num>n Then k=k-1
					strSub = Left(AB.H.Trim(s),k) & p
				Loop
			End If
		End If
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.GetStrByLen(str, n, p)
	'@ 返  回:  String (字符串)
	'@ 作  用:  截取字符宽度为x的字符串
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'@ 参数 n   : String (字符串) 截取的字符宽度
	'@ 参数 p   : String (字符串) 后缀修饰符
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Char.GetStrByLen("abcdefg中文字符", 8, "..") '返回字符串: "abcdefg.."
	'@ *****************************************************************************************

	Public Function GetStrByLen(ByVal s, ByVal n, ByVal p)
		Dim i, x, temp:temp = "":x = 0
		IF n<=0 Then: GetStrByLen = s: Exit Function: End IF '当 n=0 则取全部
		IF IsNull(s) or s="" Then: GetStrByLen="": Exit Function: End IF
		IF IsNull(s) or s="" Then Exit Function
		For i=1 to Len(s)
			IF AB.C.strAsc(Mid(s, i, 1))<0 or AB.C.strAsc(Mid(s, i, 1))>255 Then
				x = x + 2
			Else
				x = x + 1
			End IF
			IF x > n Then Exit For
			temp = temp & Mid(s, i, 1)
		Next
		GetStrByLen = temp & Cstr(p)
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.EraseHtml(str)
	'@ 返  回:  String (字符串)
	'@ 作  用:  删除html标记
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Char.EraseHtml("<p>abcd</p>") '返回字符串: "abcd"
	'@ *****************************************************************************************

	Public Function EraseHtml(Byval s)
		Dim temp, regEx, str
		If IsNull(s) Or AB.H.Trim(s)="" Then
			temp = ""
		Else
			str = s & ""
			Set regEx = New RegExp
			regEx.IgnoreCase = True
			regEx.Global = True
			regEx.Pattern = "<\/*[^<>]*>"
			str = regEx.Replace(str,"")
			regEx.Pattern = "<\/*([\w]*[\s]*)" '清除残缺html错误标记 (可能干扰页面排版 如 <span </p )
			str = regEx.Replace(str,"")
			Set regEx = Nothing
			'str = replace(str, chr(13) & chr(10), " ")
			temp = str
		End If
		EraseHtml = temp
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.BoldWord(str, word)
	'@ 返  回:  String (字符串)
	'@ 作  用:  突出显示字符串中查询到的单词的函数
	'==DESC=====================================================================================
	'@ 参数 str  : String (字符串) 原字符串
	'@ 参数 word : String (字符串) 关键词
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Char.BoldWord("hello,i like asp", "like") '返回字符串: "hello,i <font color='#FF0000'>like</font> asp"
	'@ *****************************************************************************************

	Public Function BoldWord(Byval s, Byval word)
		Dim temp, regEx, str
		If IsNull(s) Or AB.H.Trim(s)="" Then
			temp = ""
		Else
			str = s & ""
			Set regEx = New RegExp
			regEx.IgnoreCase = True
			regEx.Global = True
			regEx.Pattern = "(" & word & ")"
			str = regEx.Replace(str, "<font color='#FF0000'>$1</font>")
			Set regEx = Nothing
			temp = str
		End If
		BoldWord = str
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.ToUnicode(str)
	'@ 返  回:  String (字符串)
	'@ 作  用:  将汉字等转换为&#开头的unicode字符串形式
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'==DEMO=====================================================================================
	'@ AB.C.PrintCn AB.Char.ToUnicode("中文") '返回字符串: "&#20013;&#25991;"
	'@ *****************************************************************************************

	Public Function ToUnicode(str)
		Dim i,j,c,i1,i2,u,fs,f,p
		ToUnicode=""
		p=""
		For i=1 To Len(str)
			c = Mid(str,i,1)
			j = Ascw(c)
			IF j<0 Then
				j = j + 65536
			End IF
			IF j>=0 and j<=128 Then
				IF p="c" Then
					ToUnicode = " " & ToUnicode
					p = "e"
				End IF
				ToUnicode = ToUnicode & c
			Else
				IF p="e" Then
					ToUnicode = ToUnicode & " "
					p = "c"
				End IF
				ToUnicode = ToUnicode & "&#"&j&";"
			End IF
		Next
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.GB2UTF(str)
	'@ 返  回:  String (字符串)
	'@ 作  用:  汉字转换为UTF-8
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'==DEMO=====================================================================================
	'@ AB.C.PrintCn AB.Char.GB2UTF("中文") '返回字符串: "&#x4E2D;&#x6587;"
	'@ *****************************************************************************************

	Public Function GB2UTF(Byval s)
		Dim str:str = s
		Dim temp:temp=""
		Dim I,ostr
		For I=1 to Len(str)
			ostr = Mid(str,I,1)
			temp = temp & chr(38)
			temp = temp & chr(35)
			temp = temp & chr(120)
			temp = temp & Hex(Ascw(ostr))
			temp = temp & chrW(59)
		Next
		GB2UTF = temp
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.UTF2GB(str)
	'@ 返  回:  String (字符串)
	'@ 作  用:  utf-8转gb2312
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'==DEMO=====================================================================================
	'@ AB.C.PrintCn AB.Char.UTF2GB(AB.Char.GB2UTF("中文"))
	'@ *****************************************************************************************

	Public Function UTF2GB(Byval s)
		Dim str:str = s
		Dim temp:temp=""
		IF IsNull(str) Or str="" Then:UTF2GB="":Exit Function:End IF
		str = Replace(str, "+", "%20")
		For Dig=1 To Len(str)
			IF Mid(str,Dig,1) = "%" Then
				IF LCase(Mid(str,Dig+1,1))="e" Then
					temp = temp & ConvChinese(Mid(str,Dig,9))
					Dig = Dig + 8
				Else
					temp = temp & Chr(eval("&h"+mid(str,Dig+1,2)))
					Dig = Dig + 2
				End IF
			Else
				temp = temp & Mid(str,Dig,1)
			End IF
		Next
		UTF2GB = temp
	End Function

	Public Function ConvChinese(x)
		Dim A:A=split(Mid(x,2),"%")
		Dim I,J:I=0:J=0
		For I=0 To Ubound(A)
			A(I) = c16to2(A(I))
		Next
		Dim UnicodeStr,DigS,temp
		For I=0 To Ubound(A)-1
			DigS = Instr(A(I),"0")
			UnicodeStr = ""
			For J=1 To DigS-1
				IF J=1 Then
					A(I) = Right(A(I),Len(A(I))-DigS)
					UnicodeStr = UnicodeStr & A(I)
				Else
					I=I+1
					A(I) = Right(A(I),Len(A(I))-2)
					UnicodeStr = UnicodeStr & A(I)
				End IF
			Next
			IF Len(c2to16(UnicodeStr))=4 Then
				temp = temp & ChrW(int("&H" & c2to16(UnicodeStr)))
			Else
				temp = temp & Chr(int("&H" & c2to16(UnicodeStr)))
			End IF
		Next
		ConvChinese = temp
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.Bin2Hex(str)
	'@ 返  回:  String (字符串) 取得16进制值的字符串
	'@ 作  用:  2进制转为16进制
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'==DEMO=====================================================================================
	'@ AB.C.PrintCn AB.Char.Bin2Hex("100001111000101100011111100000001101001110001") '返回字符串为："10F163F01A71"
	'@ *****************************************************************************************

	Public Function Bin2Hex(Byval s)
		Dim i,l,k,n,ostr,temp : s = AB.H.Trim(s) : temp = ""
		l = Len(s)
		k = CInt((l + 3) / 4)
		n = CInt(l Mod 4)
		For i = 0 To k - 1
			If i = 0 Then
				ostr = Mid(s, 1, n)
			Else
				ostr = Mid(s, n + 4 * i - 3, 4)
			End If
			If ostr <> "" Then temp = temp & B2H(ostr)
		Next
		Bin2Hex = temp
	End Function

	Function B2H(Byval s)
		Dim i,j
		j = 0
		For i = 1 To Len(s)
			j = j + CLng(Mid(s, i, 1)) * 2 ^ (Len(s) - i)
		Next
		B2H = CStr(Hex(j))
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.C2To16(str)
	'@ 返  回:  String (字符串) 2进制形式字符串转为16进制值的字符串
	'@ 作  用:  2进制转为16进制
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'==DEMO=====================================================================================
	'@ AB.C.PrintCn AB.Char.C2To16("1101") '返回字符串为："D"
	'@ AB.C.PrintCn AB.Char.C2To16("11110001") '返回字符串为："F1"
	'@ AB.C.PrintCn AB.Char.C2To16("00010001") '返回字符串为："11"
	'@ AB.C.PrintCn AB.Char.C2To16("10001") '返回字符串为："11"
	'@ *****************************************************************************************

	Public Function c2to16(x)
		Dim temp:temp=""
		' Dim I:I=1
		' For I=1 To Len(x) Step 4
			' temp = temp & hex(c2to10(Mid(x,i,4)))
		' Next
		temp = Bin2Hex(x)
		c2to16 = temp
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.C2To10(str)
	'@ 返  回:  String (字符串) 2进制形式字符串转为10进制值形式的字符串
	'@ 作  用:  2进制转为10进制
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'==DEMO=====================================================================================
	'@ AB.C.PrintCn AB.Char.C2To10("00010001") '返回字符串为："17"
	'@ AB.C.PrintCn AB.Char.C2To10("10001") '返回字符串为："17"
	'@ *****************************************************************************************

	Public Function c2to10(x)
		Dim temp:temp=0
		IF x="0" Then Exit Function
		Dim I:I=0
		For I= 0 To Len(x) -1
			IF Mid(x,Len(x)-i,1)="1" Then temp = temp + 2^(I)
		Next
		c2to10 = temp
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.C16To2(str)
	'@ 返  回:  String (字符串) 16进制形式字符串转为2进制值形式的字符串
	'@ 作  用:  16进制转为2进制
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Char.C16To2("F1") '返回字符串为："11110001"
	'@ *****************************************************************************************

	Public Function c16to2(x)
		Dim temp:temp=""
		Dim oStr
		Dim I:I=0
		For I=1 To Len(AB.H.Trim(x))
			oStr= c10to2(cint(int("&h" & Mid(x,i,1))))
			Do while Len(oStr)<4
				oStr = "0" & oStr
			Loop
			temp = temp & oStr
		Next
		c16to2 = temp
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.C10To2(str)
	'@ 返  回:  String (字符串) 为2进制值形式的字符串
	'@ 作  用:  10进制转为2进制
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'==DEMO=====================================================================================
	'@ AB.C.Print AB.Char.C10To2("17") '返回字符串为："10001"
	'@ *****************************************************************************************

	Public Function c10to2(x)
		Dim temp:temp=""
		Dim mysign:mysign=sgn(x)
		Dim K:K=abs(x)
		Dim DigS:DigS=1
		Dim I:I=0
		Dim p:p=0
		If p=1 Then
			Dim loopnum:loopnum=0
			If K >= 2 ^ 31 Then
				c10to2 = x
				Exit Function
			End If
			Do
				If (K And 2 ^ loopnum) = 2 ^ loopnum Then
					temp = "1" & temp
				Else
					temp = "0" & temp
				End If
				loopnum = loopnum + 1
			Loop Until 2 ^ loopnum > K
		Else
			Do
				IF K < 2^DigS Then
					Exit Do
				Else
					DigS = DigS + 1
				End IF
			Loop
			For I=DigS To 1 Step-1
				IF K >= 2^(i-1) Then
					K = K - 2^(i-1)
					temp = temp & "1"
				Else
					temp = temp & "0"
				End IF
			Next
		End If
		IF mysign = -1 Then temp="-" & temp
		c10to2 = temp
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.Str2Bin(str)
	'@ 返  回:  String (字符串) 将一般字符串转为二进制字符串(非普通字符串)
	'@ 作  用:  把普通字符串 转成 二进制字符串(支持中文)
	'@ 			注意：二进制字符串 与 普通字符串 是有区别的。
	'==DESC=====================================================================================
	'@ 参数 str : String (字符串) 原字符串
	'==DEMO=====================================================================================
	'@ Dim temp : temp = AB.Char.Str2Bin("abcd中文")
	'@ Response.BinaryWrite temp '用 “Response.BinaryWrite” 将一个二进制字符串写入 HTTP 输出流
	'@ *****************************************************************************************

	Public Function Str2bin(Byval s)
		Dim temp:temp=""
		Dim i,varchar,varasc,varlow,varhigh
		For i=1 To Len(s)
			varchar = Mid(s,i,1)
			varasc = Asc(varchar)
			If varasc<0 Then
				varasc = varasc + 65535
			End If
			If varasc>255 Then
				varlow = Left(Hex(Asc(varchar)),2)
				varhigh = Right(Hex(Asc(varchar)),2)
				temp = temp & ChrB("&H" & varlow) & ChrB("&H" & varhigh)
			Else
				temp = temp & ChrB(Asc(varchar))
			End If
		Next
		Str2bin = temp
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.Bin2Str(str)
	'@ 返  回:  String (普通字符串) 将二进制字符串转为普通字符串
	'@ 作  用:  把二进制字符串 转成 普通字符串
	'==DESC=====================================================================================
	'@ 参数 str : Bin String (二进制) 二进制字符串
	'==DEMO=====================================================================================
	'@ Dim temp : temp = AB.Char.Str2Bin("abcd中文")
	'@ 'Response.BinaryWrite temp '用 “Response.BinaryWrite” 将一个二进制字符串写入 HTTP 输出流
	'@ Response.Write AB.Char.Bin2Str(temp)
	'@ *****************************************************************************************

	Public Function Bin2Str(ByVal s)
		Dim i, str, clow
		For i=1 To LenB(s)
			clow = MidB(s,i,1)
			If AscB(clow)<128 Then
				str = str & Chr(AscB(clow))
			Else
				i=i+1
				If i <= LenB(s) Then str = str & Chr(AscW(MidB(s,i,1)&clow))
			End If
		Next
		Bin2Str = str
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.LShift(lThis, lBits)
	'@ 返  回:  String (字符串) 为2进制值形式的字符串
	'@ 作  用:  向左移位(移位运算)
	'==DESC=====================================================================================
	'@ 参数 lThis : Integer (整数) 原数值
	'@ 参数 lBits : Integer (整数) 向左移位移动的位数
	'==DEMO=====================================================================================
	'@ AB.C.PrintCn "200<<4 is:" & AB.Char.LShift(200, 4) '数值 200 向左移位4位
	'@ AB.C.PrintCn "<script>alert('200<<4 is:' + (200<<4));</script>"
	'@ '--以下另外一种算法：
	'@ Dim n : n = 200 : n = n * 2^4 '向左移位4位
	'@ AB.C.PrintCn "200<<4 is:" & n
	'@ *****************************************************************************************

	Public Function LShift(ByVal lThis, ByVal lBits)
		On Error Resume Next
		If Not b_init Then Init()
		If (lBits <= 0) Then
			LShift = lThis
		ElseIf (lBits > 63) Then
			' .. error ...
		ElseIf (lBits > 31) Then
			LShift = 0
		Else
			If (lThis And m_lPower2(31 - lBits)) = m_lPower2(31 - lBits) Then
				LShift = (lThis And (m_lPower2(31 - lBits) - 1)) * m_lPower2(lBits) Or m_lPower2(31)
			Else
				LShift = (lThis And (m_lPower2(31 - lBits) - 1)) * m_lPower2(lBits)
			End If
		End If
		On Error Goto 0
	End Function

	'@ *****************************************************************************************
	'@ 过程名:  AB.Char.RShift(lThis, lBits)
	'@ 返  回:  String (字符串) 为2进制值形式的字符串
	'@ 作  用:  向右移位(移位运算)
	'==DESC=====================================================================================
	'@ 参数 lThis : Integer (整数) 原数值
	'@ 参数 lBits : Integer (整数) 向右移位移动的位数
	'==DEMO=====================================================================================
	'@ AB.C.PrintCn "200>>4 is:" & AB.Char.RShift(200, 4) '数值 200 向右移位4位
	'@ AB.C.PrintCn "<script>alert('200>>4 is:' + (200>>4));</script>"
	'@ '--以下另外一种算法：
	'@ Dim n : n = 200 : n = n \ 2^4 '向右移位4位
	'@ AB.C.PrintCn "200>>4 is:" & n
	'@ *****************************************************************************************

	Public Function RShift(ByVal lThis, ByVal lBits)
		On Error Resume Next
		If Not b_init Then Init()
		If (lBits <= 0) Then
			RShift = lThis
		ElseIf (lBits > 63) Then
			' ... error ...
		ElseIf (lBits > 31) Then
			RShift = 0
		Else
			If (lThis And m_lPower2(31)) = m_lPower2(31) Then
				RShift = (lThis And &H7FFFFFFF) \ m_lPower2(lBits) Or m_lPower2(31 - lBits)
			Else
				RShift = lThis \ m_lPower2(lBits)
			End If
		End If
		On Error Goto 0
	End Function

End Class
%>