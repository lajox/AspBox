# AspBox
> #### 相关资源
* <b>官方网站 : [永久网络](http://www.19www.com) </b> :point_left:

> #### 学习交流
* 框架交流群：![](https://raw.githubusercontent.com/JackJiang2011/MobileIMSDK/master/preview/more_screenshots/others/qq_group_icon_16-16.png) `41161311` :point_left:
* bug/建议发送至邮箱：`lajox@19www.com`
* 技术支持/合作/咨询请联系作者QQ：`517544292`

# 一、简介

<b>AspBox是一个方便应用于开发人员快速开发的ASP开发框架：</b> 
* AspBox还可以进行拓展子类对象以增强自身功能。封装严谨，层层嵌套，提高了代码重复利用多次利用；
* AspBox提供了大量实用的ASP通用过程及方法和子类，可以简化大部分的ASP操作。 

> 内置MVC基础模型<br>
自由拓展类库

AspBox框架已有应用例子AppCore: 您可以下载体验：[AppCore 1.0 版本](http://git.oschina.net/lajox/AppCore)。:point_left:

# 二、使用方法

> #### (1) AspBox 核心的主要集中在Cls_AB.asp文件，所以只需要在页首引入该文件，如：

```asp
<!--#include file="inc/AspBox/Cls_AB.asp" -->
```

> #### (2) 配置AspBox相关参数(文件AB.Config.asp)，例如：

```asp
AB.BasePath = "/Inc/AspBox/"
```

> #### (3) AspBox在1.1版以上已支持MVC框架拓展，

使用方法：只需要在调用的代码前面加入： ab.use "mvc" 即可开启MVC模式， 例如：

```asp
AB.Use "Mvc"
Service.Use "Check"
AB.C.Print "当前访问的文件名：" & Service.Check.GetSelfName
```

# 三、框架说明
<b>整个框架基本由各个核心组成：</b>

* <b>ab.a.asp：</b>A模块处理Array数组；
* <b>ab.c.asp：</b>C通用函数类；
* <b>ab.cookie.asp：</b>Cookie操作类；
* <b>ab.cache.asp：</b>Catch缓存类操作；
* <b>ab.char.asp：</b>Char字符处理类；
* <b>ab.d.asp：</b>D一般函数库；
* <b>ab.db.asp：</b>DB数据操作类；
* <b>ab.dbo.asp：</b>DBO数据操作对象；
* <b>ab.e.asp：</b>E加密模块(包含了Md5,Base64,SHA-1加密及收集了一些加密函数块)；
* <b>ab.error.asp：</b>Error错误处理块；
* <b>ab.form.asp：</b>Form表单处理块；
* <b>ab.fso.asp：</b>Fso操作类操作；
* <b>ab.h.asp：</b>H系统函数集成块；
* <b>ab.html.asp：</b>Html控件生成模块；
* <b>ab.http.asp：</b>Http对XMLHttp处理块；
* <b>ab.json.asp：</b>Json处理块；
* <b>ab.jslib.asp：</b>jsLib(JS脚本核心引用操作)；
* <b>ab.key.asp：</b>Key字典库操作类；
* <b>ab.list.asp：</b>List处理各种List对象；
* <b>ab.mail.asp：</b>Mail邮件处理块；
* <b>ab.pager.asp：</b>Pager分页类；
* <b>ab.py.asp, ab.pyo.asp, ab.pyz.asp：</b>PY拼音转换模块；
* <b>ab.rnd.asp：</b>Rnd随机数函数块；
* <b>ab.session.asp：</b>Session操作类；
* <b>ab.sc.asp：</b>Sc脚本执行操作模块；
* <b>ab.time.asp：</b>Time时间处理块；
* <b>ab.tpl.asp：</b>Tpl模板类；
* <b>ab.up.asp, ab.upload.asp：</b>Upload上传处理块；
* <b>ab.url.asp：</b>Url处理块；
* <b>ab.x.asp：</b>X扩展块；
* <b>ab.xml.asp：</b>Xml处理XML块；

* 以及其他一些调用方法如aspjpeg组件操作,无惧上传类,艾恩上传类等


# 四、性能测试
压力测试表明，AspBox可以承载百万条数据。

# 五、应用案例
#### ① AppCore是以AspBox框架为核心构建的基于MVC模式的APP应用：
> AppCore演示程序正在完善，可供下载：[点击下载体验](http://git.oschina.net/lajox/AppCore) :point_left:
